<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\ComponentController;

Route::group(
    [
        'prefix' => 'components',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function() {
        Route::get('/get-by-id', ComponentController::class . '@getById');
        Route::get('/get-list', ComponentController::class . '@getList');
        Route::post('/create', ComponentController::class . '@create');
        Route::post('/update', ComponentController::class . '@update');
        Route::post('/delete', ComponentController::class . '@delete');
    }
);
