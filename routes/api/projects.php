<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\ProjectController;

Route::group(
    [
        'prefix' => 'projects',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function () {
        Route::get('/get-by-id', ProjectController::class . '@getById');
        Route::get('/get-list', ProjectController::class . '@getList');
        Route::get('/get-simple-list', ProjectController::class . '@getSimpleList');
        Route::post('/create', ProjectController::class . '@create');
        Route::post('/update', ProjectController::class . '@update');
        Route::post('/add-members', ProjectController::class . '@addMembers');
        Route::post('/remove-members', ProjectController::class . '@removeMembers');
    }
);
