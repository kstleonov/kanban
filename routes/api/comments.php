<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\CommentController;

Route::group(
    [
        'prefix' => 'comments',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function () {
        Route::post('/create', CommentController::class . '@create');
        Route::post('/update', CommentController::class . '@update');
    }
);
