<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\TaskTypeController;

Route::group(
    [
        'prefix' => 'task-types',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function () {
        Route::get('/get-list', TaskTypeController::class . '@getList');
    }
);
