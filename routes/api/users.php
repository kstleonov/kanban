<?php

use App\Http\Controllers\Api\UserController;
use App\Middleware\ApiWrapperMiddleware;

Route::group(
    [
        'prefix' => 'users',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function() {
        Route::get('/get-by-id', UserController::class . '@getById');
        Route::get('/get-list', UserController::class . '@getList');
        Route::get('/get-simple-list', UserController::class . '@getSimpleList');
        Route::post('/create', UserController::class . '@create');
        Route::post('/update', UserController::class . '@update');
    }
);
