<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\TaskStatusController;

Route::group(
    [
        'prefix' => 'task-statuses',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function () {
        Route::get('/get-list', TaskStatusController::class . '@getList');
    }
);
