<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\TagController;

Route::group(
    [
        'prefix' => 'tags',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function() {
        Route::get('/get-by-id', TagController::class . '@getById');
        Route::get('/get-list', TagController::class . '@getList');
        Route::post('/create', TagController::class . '@create');
        Route::post('/update', TagController::class . '@update');
        Route::post('/delete', TagController::class . '@delete');
    }
);
