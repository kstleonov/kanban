<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\TaskController;

Route::group(
    [
        'prefix' => 'tasks',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function () {
        Route::get('/get-by-id', TaskController::class . '@getById');
        Route::get('/get-list', TaskController::class . '@getList');
        Route::post('/create', TaskController::class . '@create');
        Route::post('/update', TaskController::class . '@update');
        Route::post('/add-observer', TaskController::class . '@addObserver');
    }
);
