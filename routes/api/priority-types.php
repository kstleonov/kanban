<?php

use App\Middleware\ApiWrapperMiddleware;
use App\Http\Controllers\Api\PriorityTypeController;

Route::group(
    [
        'prefix' => 'priority-types',
        'middleware' => [
            ApiWrapperMiddleware::class
        ]
    ],
    static function() {
        Route::get('/get-list', PriorityTypeController::class . '@getList');
    }
);
