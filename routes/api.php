<?php

require_once __DIR__ . '/api/users.php';
require_once __DIR__ . '/api/task-statuses.php';
require_once __DIR__ . '/api/priority-types.php';
require_once __DIR__ . '/api/tags.php';
require_once __DIR__ . '/api/components.php';
require_once __DIR__ . '/api/projects.php';
require_once __DIR__ . '/api/comments.php';
require_once __DIR__ . '/api/task-types.php';
require_once __DIR__ . '/api/tasks.php';

