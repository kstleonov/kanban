

# Kanban board
##Описание проекта
Проект представляет простую канбан доску. 
Сущности, которые представляет проект:
- Пользователи
- Задачи
- Проекты
- Статусы задач
- Типы задач
- Компоненты задач
- Приоритеты задач
- Тэги
- Комментарии

Документация и postman коллекция лежит по пути - ***public/docs/index.html***

##Сборка и настройка проекта

Проект использует три docker контейнера - nginx, db, php-fpm.
Dockerfile и конфигурация nginx лежат в папке ***docker***

Настройки доступа к базе данных уже прописаны в файле ***.env.example**.

Запуск копозера для установки зависимостей
```
docker run --rm -v $(pwd):/app composer install
```
Скопировать файл .evn.example в файл .env

Запустить сборку контейнера
```
docker-compose up -d
```
Запустить миграции без тестовых данных
```
docker exec -it kanban_php php artisan migrate
```

Миграции с тестовыми данными
```
docker exec -it kanban_php php artisan migrate --seed
```
