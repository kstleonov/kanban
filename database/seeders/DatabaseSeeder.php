<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\CommentTask;
use App\Models\Component;
use App\Models\ObserverTask;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\Tag;
use App\Models\Task;
use App\Models\TaskTag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = $this->makeUsers();
        $projects = $this->makeProjects();
        $this->makeProjectUsers($users, $projects);
        $this->makeComponents();
        $tasks = $this->makeTask();
        $this->makeObserverTask($users, $tasks);
        $comments = $this->makeComments();
        $this->makeCommentsTasks($comments, $tasks);
        $this->makeTags();
        $this->makeTaskTag();
    }

    private function makeUsers(): Collection
    {
        return User::factory(10)
            ->create();
    }

    private function makeProjects(): Collection
    {
        return Project::factory(2)
            ->create();
    }

    private function makeProjectUsers(Collection $users, Collection $projects)
    {
        $projectOwnersIds = $projects->pluck('owner_id');
        $usersIds = $users->pluck('id')->diff($projectOwnersIds);

        $projectUserInsert = [];

        foreach ($usersIds as $userId) {
            $projectUserInsert[] = [
                'project_id' => $projects->random()->id,
                'user_id' => $userId
            ];
        }

        ProjectUser::query()
            ->insert($projectUserInsert);
    }

    private function makeComponents()
    {
        $components = [
            [
                'title' => 'Backend',
                'description' => 'Backend component'
            ],
            [
                'title' => 'Frontend',
                'description' => 'Frontend component'
            ],
            [
                'title' => 'IOS',
                'description' => 'IOS component'
            ],
            [
                'title' => 'Android',
                'description' => 'Android component'
            ]
        ];

        $projectUsers = ProjectUser::query()
            ->limit(4)
            ->get()
            ->toArray();

        $componentsInsert = [];

        foreach ($components as $key => $component) {
            $componentsInsert[] = [
                'title' => $component['title'],
                'description' => $component['description'],
                'owner_id' => $projectUsers[$key]['user_id'],
                'project_id' => $projectUsers[$key]['project_id']
            ];
        }

        Component::query()
            ->insert($componentsInsert);
    }

    private function makeTask(): Collection
    {
        return Task::factory(2)
            ->create();
    }

    private function makeObserverTask(Collection $users, Collection $tasks)
    {
        $authorsIds = $tasks->pluck('author_id');
        $performersIds = $tasks->pluck('performer_id');
        $observersIds = $authorsIds->merge($performersIds)->unique();

        $usersIds = $users->pluck('id')->diff($observersIds);

        $observerTaskInsert = [];

        foreach ($usersIds as $userId) {
            $observerTaskInsert[] = [
                'task_id' => $tasks->random()->id,
                'observer_id' => $userId
            ];
        }

        ObserverTask::query()
            ->insert($observerTaskInsert);
    }

    private function makeComments(): Collection
    {
        return Comment::factory(10)
            ->create();
    }

    private function makeCommentsTasks(Collection $comments, Collection $tasks)
    {
        $commentsTasksInsert = [];

        /** @var Comment $comment */
        foreach ($comments as $comment) {
            $commentsTasksInsert[] = [
                'task_id' => $tasks->random()->id,
                'comment_id' => $comment->id
            ];
        }

        CommentTask::query()
            ->insert($commentsTasksInsert);
    }

    private function makeTags()
    {
        Tag::factory(5)
            ->create();
    }

    private function makeTaskTag()
    {
        TaskTag::factory(10)
            ->create();
    }
}
