<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->bigInteger('status_id');
            $table->bigInteger('project_id');
            $table->bigInteger('type_id');
            $table->bigInteger('priority_id');
            $table->bigInteger('component_id')->nullable();
            $table->bigInteger('performer_id');
            $table->bigInteger('author_id');
            $table->bigInteger('initial_time')->nullable();
            $table->bigInteger('left_time')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('task_statuses');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('type_id')->references('id')->on('task_types');
            $table->foreign('priority_id')->references('id')->on('priority_types');
            $table->foreign('component_id')->references('id')->on('components');
            $table->foreign('performer_id')->references('id')->on('users');
            $table->foreign('author_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
