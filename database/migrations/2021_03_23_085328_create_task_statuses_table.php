<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateTaskStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
        });

        DB::table('task_statuses')
            ->insert([
                [
                    'title' => 'In queue'
                ],
                [
                    'title' => 'In work'
                ],
                [
                    'title' => 'Testing'
                ],
                [
                    'title' => 'Deployment'
                ],
                [
                    'title' => 'Close'
                ]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_statuses');
    }
}
