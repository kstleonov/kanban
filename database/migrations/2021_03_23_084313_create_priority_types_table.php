<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePriorityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('priority_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
        });

        DB::table('priority_types')
            ->insert([
                [
                    'title' => 'Low'
                ],
                [
                    'title' => 'Normal'
                ],
                [
                    'title' => 'Critical'
                ],
                [
                    'title' => 'Priority'
                ]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('priority_types');
    }
}
