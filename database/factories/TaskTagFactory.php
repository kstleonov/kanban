<?php

namespace Database\Factories;

use App\Models\Tag;
use App\Models\Task;
use App\Models\TaskTag;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskTagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TaskTag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $tasks = Task::all();
        $tags = Tag::all();

        return [
            'tag_id' => $tags->random(),
            'task_id' => $tasks->random()
        ];
    }
}
