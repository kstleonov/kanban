<?php

namespace Database\Factories;

use App\Models\Component;
use App\Models\PriorityType;
use App\Models\Project;
use App\Models\Task;
use App\Models\TaskStatus;
use App\Models\TaskType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        /** @var TaskStatus $status */
        $status = TaskStatus::all()->random();

        /** @var Project $project */
        $project = Project::all()->random();

        /** @var TaskType $type */
        $type = TaskType::all()->random();

        /** @var PriorityType $priority */
        $priority = PriorityType::all()->random();

        /** @var Component $component */
        $component = Component::all()->random();

        /** @var User $performer */
        $performer = User::all()->random();

        /** @var User $author */
        $author = User::all()->random();

        return [
            'title' => $this->faker->word . 'Task',
            'description' => $this->faker->text,
            'status_id' => $status->id,
            'project_id' => $project->id,
            'type_id' => $type->id,
            'priority_id' => $priority->id,
            'component_id' => $component->id,
            'performer_id' => $performer->id,
            'author_id' => $author->id,
            'initial_time' => rand(1, 100),
            'left_time' => rand(1, 100)
        ];
    }
}
