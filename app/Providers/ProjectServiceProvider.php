<?php

namespace App\Providers;

use App\Models\Project;
use App\Models\ProjectUser;
use Illuminate\Support\ServiceProvider;

class ProjectServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //TODO Изменить создание фейковых данных с этого провайдера на Project->attach()
        Project::created(static function(Project $project) {
            $projectUser = new ProjectUser();
            $projectUser->project_id = $project->id;
            $projectUser->user_id = $project->owner_id;
            $projectUser->save();
        });
    }
}
