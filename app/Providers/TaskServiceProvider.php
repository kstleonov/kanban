<?php

namespace App\Providers;

use App\Models\ObserverTask;
use App\Models\Task;
use Illuminate\Support\ServiceProvider;

class TaskServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Task::created(static function(Task $task) {
            //TODO Создать для логики отдельный обработчик
            $insert[] = [
                'task_id' => $task->id,
                'observer_id' => $task->author_id
            ];

            if ($task->author_id !== $task->performer_id) {
                $insert[] = [
                    'task_id' => $task->id,
                    'observer_id' => $task->performer_id
                ];
            }

            ObserverTask::query()
                ->insert($insert);
        });
    }
}
