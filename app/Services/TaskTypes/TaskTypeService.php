<?php

namespace App\Services\TaskTypes;

use App\Models\TaskType;
use Illuminate\Database\Eloquent\Collection;

class TaskTypeService
{
    public function getById(int $type_id): TaskType
    {
        /** @var TaskType $taskType */
        $taskType = TaskType::query()
            ->find($type_id);

        if ($taskType === null) {
            throw new \DomainException('Task type not found');
        }

        return $taskType;
    }

    public function getLIst(): Collection
    {
        $taskTypes = TaskType::query()
            ->get();

        return $taskTypes;
    }
}
