<?php

namespace App\Services\Tasks;

use App\Models\Task;
use App\Services\Components\ComponentService;
use App\Services\Projects\ProjectService;
use App\Services\Users\UserService;
use Illuminate\Database\Eloquent\Builder;

class TaskService
{
    /** @var ComponentService */
    private $componentService;

    /** @var ProjectService */
    private $projectService;

    /** @var UserService */
    private $userService;

    public function __construct(
        ComponentService $componentService,
        ProjectService $projectService,
        UserService $userService
    )
    {
        $this->componentService = $componentService;
        $this->projectService = $projectService;
        $this->userService = $userService;
    }

    public function getById(int $user_id, int $task_id): Task
    {
        $task = Task::query()
            ->projectAvailable($user_id)
            ->where('id', $task_id)
            ->with(
                'status',
                'project',
                'type',
                'priority',
                'component',
                'performer',
                'author',
                'observers',
                'comments',
                'tags'
            )
            ->first();

        if ($task === null) {
            throw new \DomainException('Task not found');
        }

        return $task;
    }

    public function getList(
        int $user_id,
        string $search = null,
        int $status_id = null,
        int $type_id = null,
        int $priority_id = null,
        int $component_id = null
    ): Builder
    {
        $builder = Task::query()
            ->projectAvailable($user_id)
            ->with(
                'status',
                'type',
                'priority',
                'component',
                'performer',
                'author'
            );

        $this->searchBuilder($builder, $search);
        $this->filterBuilder($builder, $status_id, $type_id, $priority_id, $component_id);

        return $builder;
    }

    public function create(
        int $user_id,
        string $title,
        string $description,
        int $status_id,
        int $project_id,
        int $type_id,
        int $priority_id,
        int $performer_id,
        int $component_id = null,
        int $initial_time = null,
        int $left_time = null,
        array $tags_ids = []
    ): Task
    {
        $this->checkRelations($performer_id, $user_id, $project_id, $component_id);

        try {
            \DB::beginTransaction();

            $task = new Task();
            $task->title = $title;
            $task->description = $description;
            $task->status_id = $status_id;
            $task->project_id = $project_id;
            $task->type_id = $type_id;
            $task->priority_id = $priority_id;
            $task->performer_id = $performer_id;
            $task->author_id = $user_id;
            $task->component_id = $component_id;
            $task->initial_time = $initial_time;
            $task->left_time = $left_time;
            $task->save();

            $task->tags()->attach($tags_ids);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Task create error');
        }

        return $task;
    }

    public function update(
        int $user_id,
        int $task_id,
        string $title,
        string $description,
        int $status_id,
        int $type_id,
        int $priority_id,
        int $performer_id,
        int $component_id = null,
        int $initial_time = null,
        int $left_time = null,
        array $tags_ids = []
    ): Task
    {
        /** @var Task $task */
        $task = Task::query()
            ->where('id', $task_id)
            ->where('author_id', $user_id)
            ->first();

        if ($task === null) {
            throw new \DomainException('Task not found');
        }

        $this->checkRelations($performer_id, $user_id, $task->project_id, $component_id);

        try {
            \DB::beginTransaction();

            $task->title = $title;
            $task->description = $description;
            $task->status_id = $status_id;
            $task->type_id = $type_id;
            $task->priority_id = $priority_id;
            $task->performer_id = $performer_id;
            $task->component_id = $component_id;
            $task->initial_time = $initial_time;
            $task->left_time = $left_time;
            $task->save();

            $task->observers()->sync([$performer_id], true);
            $task->tags()->sync($tags_ids, true);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Task update error');
        }

        return $task->refresh();
    }

    public function addObserver(int $user_id, int $task_id): Task
    {
        $task = $this->getById($user_id, $task_id);

        $task->observers()->sync([$user_id]);

        return $task->refresh();
    }

    private function filterBuilder(
        Builder $builder,
        int $status_id = null,
        int $type_id = null,
        int $priority_id = null,
        int $component_id = null
    )
    {
        $builder->when(
            $status_id,
            static function (Builder $builder, $status_id) {
                $builder->where('status_id', $status_id);
            }
        )->when(
            $type_id,
            static function (Builder $builder, $type_id) {
                $builder->where('type_id', $type_id);
            }
        )->when(
            $priority_id,
            static function (Builder $builder, $priority_id) {
                $builder->where('priority_id', $priority_id);
            }
        )->when(
            $component_id,
            static function (Builder $builder, $component_id) {
                $builder->where('component_id', $component_id);
            }
        );
    }

    private function searchBuilder(Builder $builder, string $search = null)
    {
        $builder->when(
            $search,
            static function (Builder $builder, $search) {
                $builder->where('title', 'ilike', "%{$search}%")
                    ->orWhere('description', 'ilike', "%{$search}%")
                    ->orWhereHas('performer', static function (Builder $builder) use ($search) {
                        $builder->orWhere('name', 'ilike', "%{$search}%")
                            ->orWhere('surname', 'ilike', "%{$search}%")
                            ->orWhere('email', 'ilike', "%{$search}%");
                    })
                    ->orWhereHas('author', static function (Builder $builder) use ($search) {
                        $builder->orWhere('name', 'ilike', "%{$search}%")
                            ->orWhere('surname', 'ilike', "%{$search}%")
                            ->orWhere('email', 'ilike', "%{$search}%");
                    })
                    ->orWhereHas('tags', static function (Builder $builder) use ($search) {
                        $builder->orWhere('title', 'ilike', "%{$search}%");
                    });
            }
        );
    }

    private function checkRelations(int $performer_id, int $author_id, int $project_id, int $component_id = null)
    {
        $this->userService->getById($performer_id);
        $this->projectService->getById($project_id, $author_id);
        $this->projectService->getById($project_id, $performer_id);

        if ($component_id !== null) {
            $this->componentService->checkComponentProject($component_id, $project_id);
        }
    }
}
