<?php

namespace App\Services\Components;

use App\Models\Component;
use App\Services\Projects\ProjectService;
use Illuminate\Database\Eloquent\Builder;

class ComponentService
{
    /** @var ProjectService $projectService */
    private $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function getById(int $component_id, int $user_id): Component
    {
        /** @var Component $component */
        $component = Component::query()
            ->userProject($user_id)
            ->where('id', $component_id)
            ->with('owner')
            ->first();

        if ($component === null) {
            throw new \DomainException('Component not found');
        }

        return $component;
    }

    public function getList(int $user_id, string $search = null): Builder
    {
        /** @var Builder $builder */
        $builder = Component::query()
            ->userProject($user_id)
            ->when(
                $search,
                static function (Builder $builder, $search) {
                    $builder->where('title', 'ilike', "%{$search}%")
                        ->orWhereHas('owner', static function (Builder $builder) use ($search) {
                            $builder->where('name', 'ilike', "%{$search}%")
                                ->orWhere('surname', 'ilike', "%{$search}%")
                                ->orWhere('email', 'ilike', "%{$search}%");
                        });
                }
            )->with('owner');

        return $builder;
    }

    public function create(
        int $user_id,
        int $project_id,
        string $title,
        string $description = null
    ): Component
    {
        $project = $this->projectService->getById($project_id, $user_id);

        $component = new Component();
        $component->title = $title;
        $component->description = $description;
        $component->owner_id = $user_id;
        $component->project_id = $project->id;
        $component->save();

        return $component;
    }

    public function update(
        int $component_id,
        int $user_id,
        string $title,
        string $description = null,
        int $owner_id = null
    ): Component
    {
        $component = $this->getById($component_id, $user_id);

        if ($component->owner_id !== $user_id) {
            throw new \DomainException('No rights to modify component');
        }

        $component->title = $title;
        $component->description = $description;

        if (!empty($owner_id)) {
            $this->projectService->getById($component->project_id, $owner_id);
            $component->owner_id = $owner_id;
        }

        $component->save();

        return $component;
    }

    public function delete(int $component_id, int $user_id)
    {
        $component = $this->getById($component_id, $user_id);

        if ($component->owner_id !== $user_id) {
            throw new \DomainException('No rights to modify component');
        }

        try {
            $component->delete();
        } catch (\Throwable $e) {
            throw new \DomainException('Deletion failed');
        }
    }

    public function checkComponentProject(int $component_id, int $project_id)
    {
        $component = Component::query()
            ->where('id', $component_id)
            ->where('project_id', $project_id)
            ->first();

        if ($component === null) {
            throw new \DomainException('Component not available for project');
        }
    }
}
