<?php

namespace App\Services\Tags;

use App\Models\Tag;
use App\Models\TaskTag;
use Illuminate\Database\Eloquent\Builder;

class TagService
{
    public function getById(int $tag_id): Tag
    {
        /** @var Tag $tag */
        $tag = Tag::query()
            ->find($tag_id);

        if ($tag === null) {
            throw new \DomainException('Tag not found');
        }

        return $tag;
    }

    public function getList(string $search = null): Builder
    {
        $builder = Tag::query()
            ->when(
                $search,
                static function(Builder $builder, $search) {
                    $builder->where('title', 'ilike', "%{$search}%");
                }
            );

        return $builder;
    }

    public function create(string $title): Tag
    {
        $tag = new Tag();
        $tag->title = '#' . $title;
        $tag->save();

        return $tag;
    }

    public function update(int $tag_id, string $title): Tag
    {
        $tag = $this->getById($tag_id);

        $tag->title = '#' . $title;
        $tag->save();

        return $tag;
    }

    public function delete(int $tag_id)
    {
        $tag = $this->getById($tag_id);

        TaskTag::query()
            ->where('tag_id', $tag->id)
            ->delete();

        try {
            $tag->delete();
        } catch (\Exception $e) {
            throw new \DomainException('Deletion failed');
        }
    }
}
