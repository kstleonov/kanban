<?php

namespace App\Services\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class UserService
{
    public function getById(int $user_id): User
    {
        /** @var User $user */
        $user = User::query()
            ->find($user_id);

        if ($user === null) {
            throw new \DomainException('User not found');
        }

        return $user;
    }

    public function getList(string $search = null, string $sortingField = null, string $sortingOrder = 'ASC'): Builder
    {
        $builder = User::query();
        $this->sortingBuilder($builder, $sortingField, $sortingOrder);
        $this->searchBuilder($builder, $search);

        return $builder;
    }

    public function create(string $name, string $surname, string $email): User
    {
        $user = new User();
        $user->name = $name;
        $user->surname = $surname;
        $user->email = $email;
        $user->save();

        return $user;
    }

    public function update(int $user_id, string $name, string $surname, string $email): User
    {
        $user = $this->getById($user_id);

        $user->name =$name;
        $user->surname = $surname;
        $user->email = $email;
        $user->save();

        return $user;
    }

    private function sortingBuilder(Builder $builder, string $sortingField = null, string $sortingOrder = 'ASC')
    {
        $columnName = null;

        switch ($sortingField) {
            case 'id':
                $columnName = 'id';
                break;
            case 'name':
                $columnName = 'name';
                break;
            case 'surname':
                $columnName = 'surname';
                break;
            case 'email':
                $columnName = 'email';
        }

        $builder->when(
            $columnName,
            static function(Builder $builder, $columnName) use ($sortingOrder) {
                $builder->orderBy($columnName, $sortingOrder);
            }
        );
    }

    private function searchBuilder(Builder $builder, string $search = null)
    {
        $builder->when(
            $search,
            static function (Builder $builder, $search) {
                $builder->where(
                    static function(Builder $builder) use ($search) {
                        $builder->orWhere('id', 'ilike', "%{$search}%")
                            ->orWhere('name', 'ilike', "%{$search}%")
                            ->orWhere('surname', 'ilike', "%{$search}%")
                            ->orWhere('email', 'ilike', "%{$search}%");
                    }
                );
            }
        );
    }
}
