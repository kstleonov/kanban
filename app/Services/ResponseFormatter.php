<?php

namespace App\Services;

use Illuminate\Http\Request;
use Throwable;

class ResponseFormatter
{
    /**
     * @param Request $request
     * @param $data
     * @param array $context
     *
     * @return array
     */
    public function format(Request $request, $data, array $context = []): array
    {
        $result = [
            'command' => $request->getPathInfo(),
            'message' => '',
            'error' => 0,
            'data' => $data,
            'token' => $request->get('token', 'undefined')
        ];

        return array_merge($result, $context);
    }

    /**
     * @param Request $request
     * @param Throwable $exception
     *
     * @return array
     */
    public function formatException(Request $request, $exception)
    {
        $result = [
            'command' => $request->getPathInfo(),
            'message' => $exception->getMessage(),
            'error' => 1,
            'data' => null,
            'details' => [
                'http_code' =>200
            ]
        ];

        $appEnv = env('APP_ENV');
        if ($appEnv === 'local' || $appEnv === 'dev') {
            $result['exception'] = [
                'code'            => $exception->getCode(),
                'message'         => $exception->getMessage(),
                'file'            => $exception->getFile(),
                'line'            => $exception->getLine(),
                'exception_class' => get_class($exception),
            ];
        }

        return $this->format($request, [], $result);
    }
}
