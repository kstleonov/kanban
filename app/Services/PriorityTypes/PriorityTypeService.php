<?php

namespace App\Services\PriorityTypes;

use App\Models\PriorityType;
use Illuminate\Database\Eloquent\Collection;

class PriorityTypeService
{
    public function getById(int $priority_id): PriorityType
    {
        /** @var PriorityType $priorityType */
        $priorityType = PriorityType::query()
            ->find($priority_id);

        if ($priorityType === null) {
            throw new \DomainException('Priority type not found');
        }

        return $priorityType;
    }

    public function getList(): Collection
    {
        $priorityTypes = PriorityType::query()
            ->get();

        return $priorityTypes;
    }
}
