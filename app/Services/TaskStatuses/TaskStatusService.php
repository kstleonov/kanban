<?php

namespace App\Services\TaskStatuses;

use App\Models\TaskStatus;
use Illuminate\Database\Eloquent\Collection;

class TaskStatusService
{
    public function getById(int $status_id): TaskStatus
    {
        /** @var TaskStatus $taskStatus */
        $taskStatus = TaskStatus::query()
            ->find($status_id);

        if ($taskStatus === null) {
            throw new \DomainException('Status not found');
        }

        return $taskStatus;
    }

    public function getList(): Collection
    {
        $taskStatuses = TaskStatus::query()
            ->get();

        return $taskStatuses;
    }
}
