<?php

namespace App\Services\Comments;

use App\Models\Comment;
use App\Services\Tasks\TaskService;

class CommentService
{
    /** @var TaskService */
    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function create(int $user_id, int $task_id, string $text): Comment
    {
        $this->taskService->getById($user_id, $task_id);

        try {
            \DB::beginTransaction();

            $comment = new Comment();
            $comment->author_id = $user_id;
            $comment->text = $text;
            $comment->save();

            $comment->task()->attach($task_id);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Error while creating comment');
        }

        return $comment;
    }

    public function update(int $user_id, int $comment_id, string $text): Comment
    {
        /** @var Comment $comment */
        $comment = Comment::query()
            ->where('id', $comment_id)
            ->where('author_id', $user_id)
            ->first();

        if ($comment === null) {
            throw new \DomainException('Comment not found');
        }

        $comment->text = $text;
        $comment->save();

        return $comment;
    }
}
