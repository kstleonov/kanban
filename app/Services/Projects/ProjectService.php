<?php

namespace App\Services\Projects;

use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class ProjectService
{
    public function getById(int $project_id, int $user_id): Project
    {
        /** @var Project $project */
        $project = Project::query()
            ->projectAvailable($user_id)
            ->where('id', $project_id)
            ->with('owner', 'users')
            ->first();

        if ($project === null) {
            throw new \DomainException('Project not found');
        }

        return $project;
    }

    public function getList(
        int $user_id,
        string $search = null,
        string $sortingField = null,
        $sortingOrder = 'ASC'
    ): Builder
    {
        $builder = Project::query()
            ->projectAvailable($user_id)
            ->with('owner', 'users');

        $this->searchBuilder($builder, $search);
        $this->sortingBuilder($builder, $sortingField, $sortingOrder);

        return $builder;
    }

    public function getSimpleList(int $user_id, string $search = null): Builder
    {
        $builder = Project::query()
            ->projectAvailable($user_id);

        $this->searchBuilder($builder, $search);

        return $builder;
    }

    public function create(int $user_id, string $title): Project
    {
        $project = new Project();
        $project->title = $title;
        $project->owner_id = $user_id;
        $project->save();

        return $project;
    }

    public function update(int $user_id, int $project_id, string $title, int $owner_id = null): Project
    {
        $project = $this->getById($project_id, $user_id);

        if ($project->owner_id !== $user_id) {
            throw new \DomainException('No rights to modify project');
        }

        try {
            \DB::beginTransaction();

            $project->title = $title;
            $project->owner_id = $owner_id ?? $project->owner_id;
            $project->save();

            if (!empty($owner_id)) {
                $project->users()->sync([$owner_id], false);
            }

            \DB::commit();
        }catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Project update error');
        }

        return $project->refresh();
    }

    public function addMembers(int $user_id, int $project_id, array $members_ids = []): Project
    {
        $project = $this->getById($project_id, $user_id);

        if ($project->owner_id !== $user_id) {
            throw new \DomainException('No rights to modify project');
        }

        try {
            \DB::beginTransaction();

            $usersIds = User::query()
                ->whereIn('id', $members_ids)
                ->pluck('id')
                ->toArray();

            $project->users()->sync($usersIds, false);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Error while adding members');
        }
        return $project->refresh();
    }

    public function removeMembers(int $user_id, int $project_id, array $members_ids = []): Project
    {
        $project = $this->getById($project_id, $user_id);

        if ($project->owner_id !== $user_id) {
            throw new \DomainException('No rights to modify project');
        }

        try {
            \DB::beginTransaction();

            $usersIds = ProjectUser::query()
                ->whereIn('user_id', $members_ids)
                ->pluck('user_id')
                ->toArray();

            if (in_array($user_id, $usersIds)) {
                throw new \DomainException('Deleted member is owner');
            }

            $project->users()->detach($usersIds);

            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollBack();

            throw new \DomainException('Error while deleting members');
        }
        return $project->refresh();
    }

    private function searchBuilder(Builder $builder, string $search = null)
    {
        $builder->when(
            $search,
            static function (Builder $builder, $search) {
                $builder->where('title', 'ilike', "%{$search}%")
                    ->orWhereHas('owner', static function (Builder $builder) use ($search) {
                        $builder->where('name', 'ilike', "%{$search}%")
                            ->orWhere('surname', 'ilike', "%{$search}%")
                            ->orWhere('email', 'ilike', "%{$search}%");
                    });
            }
        );
    }

    private function sortingBuilder(Builder $builder, string $sortingField = null, string $sortingOrder = 'ASC')
    {
        $columnName = null;

        switch ($sortingField) {
            case 'id':
                $columnName = 'id';
                break;
            case 'title':
                $columnName = 'title';
                break;
        }

        $builder->when(
            $columnName,
            static function(Builder $builder, $columnName) use ($sortingOrder) {
                $builder->orderBy($columnName, $sortingOrder);
            }
        );
    }
}
