<?php

namespace App\Resources\Tasks;

use App\Models\Task;
use App\Resources\Comments\CommentResource;
use App\Resources\Components\ComponentResource;
use App\Resources\PriorityTypes\PriorityTypeResource;
use App\Resources\Projects\ProjectResource;
use App\Resources\Tags\TagResource;
use App\Resources\TaskStatuses\TaskStatusResource;
use App\Resources\TaskTypes\TaskTypeResource;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TaskResource
 * @package App\Resources\Tasks
 *
 * @mixin Task
 */
class TaskResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'status' => TaskStatusResource::make($this->status),
            'project' => ProjectResource::make($this->project),
            'type' => TaskTypeResource::make($this->type),
            'priority' => PriorityTypeResource::make($this->priority),
            'component' => ComponentResource::make($this->component),
            'performer' => UserResource::make($this->performer),
            'author' => UserResource::make($this->author),
            'initial_time' => $this->initial_time,
            'left_time' => $this->left_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'observers' => UserResource::collection($this->observers),
            'comments' => CommentResource::collection($this->comments),
            'tags' => TagResource::collection($this->tags)
        ];
    }
}
