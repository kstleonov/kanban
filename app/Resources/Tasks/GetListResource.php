<?php

namespace App\Resources\Tasks;

use App\Models\Task;
use App\Resources\Components\ComponentResource;
use App\Resources\PriorityTypes\PriorityTypeResource;
use App\Resources\TaskStatuses\TaskStatusResource;
use App\Resources\TaskTypes\TaskTypeResource;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetListResource
 * @package App\Resources\Tasks
 *
 * @mixin Task
 */
class GetListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => TaskStatusResource::make($this->status),
            'type' => TaskTypeResource::make($this->type),
            'priority' => PriorityTypeResource::make($this->priority),
            'component' => ComponentResource::make($this->component),
            'performer' => UserResource::make($this->performer),
            'author' => UserResource::make($this->author),
        ];
    }
}
