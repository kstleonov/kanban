<?php

namespace App\Resources\Components;

use App\Models\Component;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetByIdResource
 * @package App\Resources\Components
 *
 * @mixin Component
 */
class ComponentResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'owner' => UserResource::make($this->owner)
        ];
    }
}
