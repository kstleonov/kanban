<?php

namespace App\Resources\TaskTypes;

use App\Models\TaskType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TaskTypeResource
 * @package App\Resources\TaskTypes
 *
 * @mixin TaskType
 */
class TaskTypeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
