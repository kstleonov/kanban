<?php

namespace App\Resources\PriorityTypes;

use App\Models\PriorityType;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetListResource
 * @package App\Resources\PriorityTypes
 *
 * @mixin PriorityType
 */
class PriorityTypeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
