<?php

namespace App\Resources\Projects;

use App\Models\Project;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetListResource
 * @package App\Resources\Projects
 *
 * @mixin Project
 */
class GetListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'owner' => UserResource::make($this->owner)
        ];
    }
}
