<?php

namespace App\Resources\Projects;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetSimpleListResource
 * @package App\Resources\Projects
 *
 * @mixin Project
 */
class GetSimpleListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
