<?php

namespace App\Resources\Projects;

use App\Models\Project;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProjectResource
 * @package App\Resources\Projects
 *
 * @mixin Project
 */
class ProjectResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'owner' => UserResource::make($this->owner),
            'members' => UserResource::collection($this->users)
        ];
    }
}
