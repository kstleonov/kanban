<?php

namespace App\Resources\Comments;

use App\Models\Comment;
use App\Resources\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CommentResource
 * @package App\Resources\Comments
 *
 * @mixin Comment
 */
class CommentResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'author' => UserResource::make($this->author),
            'text' => $this->text
        ];
    }
}
