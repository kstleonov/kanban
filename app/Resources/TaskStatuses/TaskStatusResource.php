<?php

namespace App\Resources\TaskStatuses;

use App\Models\TaskStatus;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetListResource
 * @package App\Resources\TaskStatuses
 *
 * @mixin TaskStatus
 */
class TaskStatusResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title
        ];
    }
}
