<?php

namespace App\Resources;

use App\Services\ResponseFormatter;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class ApiResource extends JsonResource
{
    /**
     * @var ResponseFormatter
     */
    private $formatter;

    public function __construct($resource)
    {
        parent::__construct($resource);
        self::withoutWrapping();
        /** @var ResponseFormatter formatter */
        $this->formatter = App::get(ResponseFormatter::class);
    }

    public function toArray($request)
    {
        return $this->formatter->format($request, $this->resource);
    }
}
