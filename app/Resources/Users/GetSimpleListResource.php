<?php

namespace App\Resources\Users;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GetSimpleListResource
 * @package App\Resources\Users
 *
 * @mixin User
 */
class GetSimpleListResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname
        ];
    }
}
