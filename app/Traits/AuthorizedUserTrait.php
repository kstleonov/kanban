<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;

trait AuthorizedUserTrait
{
    /**
     * @return User
     * @throws UnauthorizedException
     */
    protected function user(): User
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user === null) {
            throw new UnauthorizedException('Unauthorized');
        }

        return $user;
    }
}
