<?php

namespace App\Middleware;

use App\Resources\ApiResource;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ApiWrapperMiddleware
{
    public function handle($request, Closure $next)
    {
        /** @var Response $response */
        $response = $next($request);

        if (isset($response->exception) && $response->exception instanceof \DomainException) {
            return $response;
        }

        if ($response instanceof JsonResponse) {
            $original = json_decode($response->content(), true);

            if ($response->headers->has('X-Wrap')) {
                return $response;
            }

            return response()->json(new ApiResource($original))->header('X-Wrap', '1');
        }

        return response()->json(new ApiResource($response->getOriginalContent()))->header('X-Wrap', '1');
    }
}
