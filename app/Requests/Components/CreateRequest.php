<?php

namespace App\Requests\Components;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Components
 *
 * @property string $title
 * @property string $description
 * @property int $project_id
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['string', 'required'],
            'description' => ['string', 'nullable'],
            'project_id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => 'Component title',
            'description' => 'Component description',
            'project_id' => 'Project'
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }


    public function getProjectId(): int
    {
        return $this->project_id;
    }
}
