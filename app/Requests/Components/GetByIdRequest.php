<?php

namespace App\Requests\Components;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetById
 * @package App\Requests\Components
 *
 * @property int $id
 */
class GetByIdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Component ID'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
