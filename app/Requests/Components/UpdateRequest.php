<?php

namespace App\Requests\Components;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Requests\Components
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $owner_id
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'title' => ['string', 'required'],
            'description' => ['string', 'nullable'],
            'owner_id' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Component ID',
            'title' => 'Component title',
            'description' => 'Component description',
            'owner_id' => 'New component owner'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getOwnerId()
    {
        return $this->owner_id;
    }
}
