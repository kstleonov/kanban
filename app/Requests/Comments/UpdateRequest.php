<?php

namespace App\Requests\Comments;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Requests\Comments
 *
 * @property int $id
 * @property string $text
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'text' => ['string', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Comment ID',
            'text' => 'Comment text'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
