<?php

namespace App\Requests\Comments;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Comments
 *
 * @property int $task_id
 * @property string $text
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'task_id' => ['int', 'required'],
            'text' => ['string', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'task_id' => 'Task`s comment',
            'text' => 'Comment`s text'
        ];
    }

    public function getTaskId(): int
    {
        return $this->task_id;
    }

    public function getText(): string
    {
        return $this->text;
    }
}
