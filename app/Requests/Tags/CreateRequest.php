<?php

namespace App\Requests\Tags;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Tags
 *
 * @property string $title
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['string', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => 'Tag title'
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
