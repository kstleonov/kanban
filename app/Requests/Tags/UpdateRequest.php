<?php

namespace App\Requests\Tags;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Requests\Tags
 *
 * @property int $id
 * @property string $title
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'title' => ['string', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Tag ID',
            'title' => 'Tag title'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
