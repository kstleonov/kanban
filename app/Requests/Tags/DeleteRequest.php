<?php

namespace App\Requests\Tags;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DeleteRequest
 * @package App\Requests\Tags
 *
 * @property int $id
 */
class DeleteRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Tag ID'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
