<?php

namespace App\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class GetListRequest
 * @package App\Requests\Projects
 *
 * @property string $search
 * @property array $sorting
 * @property int $per_page
 */
class GetListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => ['string', 'nullable'],
            'sorting' => ['array', 'nullable'],
            'sorting.order' => ['string', Rule::in(['asc', 'desc']), 'nullable'],
            'sorting.field' => ['string', 'nullable'],
            'per_page' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'search' => 'Search string',
            'sorting.order' => 'Sorting order',
            'sorting.field' => 'Sorting field',
            'per_page' => 'Per page'
        ];
    }

    public function getSearchString()
    {
        return $this->search;
    }

    public function getField($key, $default = null)
    {
        return $this->sorting[$key] ?? $default;
    }

    public function getSortingField()
    {
        return $this->getField('field');
    }

    public function getSortingOrder(): string
    {
        return $this->getField('order', 'ASC');
    }

    public function getPerPage($default = 10): int
    {
        return $this->per_page ?? $default;
    }
}
