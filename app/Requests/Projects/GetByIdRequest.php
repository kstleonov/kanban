<?php

namespace App\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetByIdRequest
 * @package App\Requests\Projects
 *
 * @property int $id
 */
class GetByIdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Project id'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
