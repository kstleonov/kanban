<?php

namespace App\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Requests\Projects
 *
 * @property int $id
 * @property int $title
 * @property int $owner_id
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'title' => ['string', 'required'],
            'owner_id' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Project ID',
            'title' => 'Project title',
            'owner_id' => 'New project owner'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getOwnerId()
    {
        return $this->owner_id;
    }
}
