<?php

namespace App\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Projects
 *
 * @property string $title
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['string', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => 'Project title'
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
