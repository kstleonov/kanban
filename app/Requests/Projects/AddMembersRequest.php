<?php

namespace App\Requests\Projects;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AddMembersRequest
 * @package App\Requests\Projects
 *
 * @property int $id
 * @property array $members
 */
class AddMembersRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'members' => ['array', 'nullable'],
            'members.*' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Project ID',
            'members' => 'New members IDs'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMembers(): array
    {
        return $this->members ?? [];
    }
}
