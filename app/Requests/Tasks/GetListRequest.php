<?php

namespace App\Requests\Tasks;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetListRequest
 * @package App\Requests\Tasks
 *
 * @property string $search
 * @property int $status_id
 * @property int $type_id
 * @property int $priority_id
 * @property int $component_id
 * @property int $per_page
 */
class GetListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => ['string', 'nullable'],
            'status_id' => ['int', 'nullable'],
            'type_id' => ['int', 'nullable'],
            'priority_id' => ['int', 'nullable'],
            'component_id' => ['int', 'nullable'],
            'per_page' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'search' => 'Search string',
            'status_id' => 'Task status',
            'type_id' => 'Task type',
            'priority_id' => 'Task priority',
            'component_id' => 'Task component',
            'per_page' => 'Per page'
        ];
    }

    public function getSearchString()
    {
        return $this->search;
    }

    public function getStatusId()
    {
        return $this->status_id;
    }

    public function getTypeId()
    {
        return $this->type_id;
    }

    public function getPriorityId()
    {
        return $this->priority_id;
    }

    public function getComponentId()
    {
        return $this->component_id;
    }

    public function getPerPage($default = 10): int
    {
        return $this->per_page ?? $default;
    }
}
