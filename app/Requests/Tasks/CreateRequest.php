<?php

namespace App\Requests\Tasks;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Tasks
 *
 * @property string $title
 * @property string $description
 * @property int $status_id
 * @property int $project_id
 * @property int $type_id
 * @property int $priority_id
 * @property int $performer_id
 * @property int $component_id
 * @property int $initial_time
 * @property int $left_time
 * @property array $tags_ids
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['string', 'required'],
            'description' => ['string', 'required'],
            'status_id' => ['int', 'required'],
            'project_id' => ['int', 'required'],
            'type_id' => ['int', 'required'],
            'priority_id' => ['int', 'required'],
            'performer_id' => ['int', 'required'],
            'component_id' => ['int', 'nullable'],
            'initial_time' => ['int', 'nullable'],
            'left_time' => ['int', 'nullable'],
            'tags_ids' => ['array', 'nullable'],
            'tags_ids.*' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'title' => 'Task title',
            'description' => 'Task description',
            'status_id' => 'Task status',
            'project_id' => 'Task project',
            'type_id' => 'Task type',
            'priority_id' => 'Task priority',
            'performer_id' => 'Task performer',
            'component_id' => 'Task component',
            'initial_time' => 'Task initial time',
            'left_time' => 'Task left time',
            'tags_ids' => 'Task tags'
        ];
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStatusId(): int
    {
        return $this->status_id;
    }

    public function getProjectId(): int
    {
        return $this->project_id;
    }

    public function getTypeId(): int
    {
        return $this->type_id;
    }

    public function getPriorityId(): int
    {
        return $this->priority_id;
    }

    public function getPerformerId(): int
    {
        return $this->performer_id;
    }

    public function getComponentId()
    {
        return $this->component_id;
    }

    public function getInitialTime()
    {
        return $this->initial_time;
    }

    public function getLeftTime()
    {
        return $this->left_time;
    }

    public function getTaskTags(): array
    {
        return $this->tags_ids ?? [];
    }
}
