<?php

namespace App\Requests\Tasks;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetByIdRequest
 * @package App\Requests\Tasks
 *
 * @property int $id
 */
class GetByIdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'Task ID'
        ];
    }

    public function getId(): int
    {
        return $this->id;
    }
}
