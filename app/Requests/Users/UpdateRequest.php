<?php

namespace App\Requests\Users;

use App\Requests\Users\Interfaces\UserCreateInterface;
use App\Requests\Users\Interfaces\UserUpdateInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateRequest
 * @package App\Requests\Users
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 */
class UpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required'],
            'name' => ['string', 'required'],
            'surname' => ['string', 'required'],
            'email' => ['email', 'required']
        ];
    }
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
