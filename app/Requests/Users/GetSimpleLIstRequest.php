<?php

namespace App\Requests\Users;

use App\Requests\Users\Interfaces\UserSearchInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetSimpleLIstRequest
 * @package App\Requests\Users
 *
 * @property string $search
 * @property int $per_page
 */
class GetSimpleLIstRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => ['string', 'nullable'],
            'per_page' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'search' => 'Search string',
            'per_page' => 'Per page'
        ];
    }

    public function getSearchString()
    {
        return $this->search;
    }

    public function getPerPage($default = 10): int
    {
        return $this->per_page ?? $default;
    }
}
