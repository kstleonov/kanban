<?php

namespace App\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetByIdRequest
 * @package App\Requests\Users
 *
 * @property int $id
 */
class GetByIdRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['int', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'id' => 'User ID'
        ];
    }

    public function getId()
    {
        return $this->id;
    }
}
