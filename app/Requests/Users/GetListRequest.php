<?php

namespace App\Requests\Users;

use App\Requests\Users\Interfaces\UserSearchInterface;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class GetListRequest
 * @package App\Requests\Users
 *
 * @property string $search
 * @property array $sorting
 * @property int $per_page
 */
class GetListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => ['string', 'nullable'],
            'sorting' => ['array', 'nullable'],
            'sorting.order' => ['string', Rule::in(['asc', 'desc']), 'nullable'],
            'sorting.field' => ['string', 'nullable'],
            'per_page' => ['int', 'nullable']
        ];
    }

    public function attributes(): array
    {
        return [
            'search' => 'Search string',
            'sorting.order' => 'Sorting order',
            'sorting.field' => 'Sorting field',
            'per_page' => 'Per page'
        ];
    }

    public function getSearchString()
    {
        return $this->search;
    }

    public function getPerPage($default = 10): Int
    {
        return $this->per_page ?? $default;
    }

    private function getField($field, $default = null)
    {
        return $this->sorting[$field] ?? $default;
    }

    public function getSortingOrder()
    {
        return $this->getField('order', 'asc');
    }

    public function getSortingField()
    {
        return $this->getField('field');
    }
}
