<?php

namespace App\Requests\Users;

use App\Requests\Users\Interfaces\UserCreateInterface;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Requests\Users
 *
 * @property string $name
 * @property string $surname
 * @property string $email
 */
class CreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'surname' => ['string', 'required'],
            'email' => ['email', 'required']
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => 'User name',
            'surname' => 'User surname',
            'email' => 'User email'
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }
}
