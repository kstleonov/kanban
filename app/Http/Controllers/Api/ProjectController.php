<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Projects\AddMembersRequest;
use App\Requests\Projects\CreateRequest;
use App\Requests\Projects\GetByIdRequest;
use App\Requests\Projects\GetListRequest;
use App\Requests\Projects\RemoveMembersRequest;
use App\Requests\Projects\UpdateRequest;
use App\Resources\Projects\GetListResource;
use App\Resources\Projects\GetSimpleListResource;
use App\Resources\Projects\ProjectResource;
use App\Services\Projects\ProjectService;

/**
 * Class ProjectController
 * @package App\Http\Controllers\Api
 *
 * @group Projects
 * Методы для работы с проектами
 */
class ProjectController extends Controller
{
    /**
     * Get By Id
     * Получить проект по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID проекта Example: 1
     *
     * @responseFile api/projects/get-by-id-response.json
     *
     * @param GetByIdRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function getById(GetByIdRequest $request, ProjectService $service)
    {
        $project = $service->getById($request->getId(), $this->user()->getId());

        ProjectResource::wrap('project');

        return ProjectResource::make($project);
    }

    /**
     * Get List
     * Получить список проектов
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: Test
     * @queryParam sorting.order string Порядок сортировки Example: desc
     * @queryParam sorting.field string Поле для сортировки Example: title
     * @queryParam per_page int Кол-во элементов на странице Example: 25
     *
     * @responseFile api/projects/get-list-response.json
     *
     * @param GetListRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function getList(GetListRequest $request, ProjectService $service)
    {
        $projects = $service->getList(
            $this->user()->getId(),
            $request->getSearchString(),
            $request->getSortingField(),
            $request->getSortingOrder()
        );

        GetListResource::wrap('projects');

        return GetListResource::collection($projects->paginate($request->getPerPage()));
    }

    /**
     * Get Simple List
     * Получить простой список проектов
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: Test
     * @queryParam per_page int Кол-во элементов на странице Example: 25
     *
     * @responseFile api/projects/get-simple-list-response.json
     *
     * @param GetListRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function getSimpleList(GetListRequest $request, ProjectService $service)
    {
        $projects = $service->getSimpleList($this->user()->getId(), $request->getSearchString());

        GetSimpleListResource::wrap('projects');

        return GetSimpleListResource::collection($projects->paginate($request->getPerPage()));
    }

    /**
     * Create
     * Создать проект
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam title required string Назваие проекта Example: Test Project
     *
     * @responseFile api/projects/create-response.json
     *
     * @param CreateRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, ProjectService $service)
    {
        $project = $service->create($this->user()->getId(), $request->getTitle());

        ProjectResource::wrap('project');

        return ProjectResource::make($project);
    }

    /**
     * Update
     * Обновить проект
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID проекта Example: 3
     * @queryParam title required string Название проекта Example: New Project
     * @queryParam owner_id int ID нового владельца проекта Example: 10
     *
     * @responseFile api/projects/update-response.json
     *
     * @param UpdateRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, ProjectService $service)
    {
        $project = $service->update(
            $this->user()->getId(),
            $request->getId(),
            $request->getTitle(),
            $request->getOwnerId()
        );

        ProjectResource::wrap('project');

        return ProjectResource::make($project);
    }

    /**
     * Add Members
     * Добавить пользователей в проект
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID проекта Example: 3
     * @queryParam members array ID добавляемых в проект пользователей Example: [1, 10, 4]
     *
     * @responseFile api/projects/add-members-response.json
     *
     * @param AddMembersRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function addMembers(AddMembersRequest $request, ProjectService $service)
    {
        $project = $service->addMembers($this->user()->getId(), $request->getId(), $request->getMembers());

        ProjectResource::wrap('project');

        return ProjectResource::make($project);
    }

    /**
     * Remove Members
     * Удалить пользователей из проекта
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID проекта Example: 3
     * @queryParam members array ID удаляемых в проект пользователей Example: [1, 3]
     *
     * @responseFile api/projects/remove-members-response.json
     *
     * @param RemoveMembersRequest $request
     * @param ProjectService $service
     *
     * @return mixed
     */
    public function removeMembers(RemoveMembersRequest $request, ProjectService $service)
    {
        $project = $service->removeMembers($this->user()->getId(), $request->getId(), $request->getMembers());

        ProjectResource::wrap('project');

        return ProjectResource::make($project);
    }
}
