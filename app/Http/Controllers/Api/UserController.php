<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Users\CreateRequest;
use App\Requests\Users\GetByIdRequest;
use App\Requests\Users\GetListRequest;
use App\Requests\Users\GetSimpleLIstRequest;
use App\Requests\Users\UpdateRequest;
use App\Resources\Users\GetSimpleListResource;
use App\Resources\Users\UserResource;
use App\Services\Users\UserService;

/**
 * Class UserController
 * @package App\Http\Controllers\Api
 * @group Users
 *
 * Методы для работы с пользователями
 *
 */
class UserController extends Controller
{
    /**
     * Get By Id
     * Получить пользователя по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID пользовтеля Example: 1
     *
     * @responseFile api/users/get-by-id-response.json
     *
     * @param GetByIdRequest $request
     * @param UserService $service
     *
     * @return mixed
     */
    public function getById(GetByIdRequest $request, UserService $service)
    {
        $user = $service->getById($request->getId());

        UserResource::wrap('user');

        return UserResource::make($user);
    }

    /**
     * Get List
     * Получить список пользователей
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: John
     * @queryParam sorting array Колонки для сортировки
     * @queryParam sorting.order string Порядок сортировки Example: ASC
     * @queryParam sorting.field string Поле для сортировки Example: name
     * @queryParam per_page int Кол-во возвращаемых записей Example: 10
     *
     * @responseFile api/users/get-list-response.json
     *
     * @param GetListRequest $request
     * @param UserService $service
     *
     * @return mixed
     */
    public function getList(GetListRequest $request, UserService $service)
    {
        $users = $service->getList(
            $request->getSearchString(),
            $request->getSortingField(),
            $request->getSortingOrder()
        );

        UserResource::wrap('users');

        return UserResource::collection($users->paginate($request->getPerPage()));
    }

    /**
     * Get Simple List
     * Получить простой список пользователей
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: John
     * @queryParam per_page int Кол-во возвращаемых записей Example: 10
     *
     * @responseFile api/users/get-simple-list-response.json
     *
     * @param GetSimpleLIstRequest $request
     * @param UserService $service
     *
     * @return mixed
     */
    public function getSimpleList(GetSimpleLIstRequest $request, UserService $service)
    {
        $users = $service->getList($request->getSearchString());

        GetSimpleListResource::wrap('users');

        return GetSimpleListResource::collection($users->paginate($request->getPerPage()));
    }

    /**
     * Create
     * Создать пользователя
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam name required string Имя пользователя Example: John
     * @queryParam surname required string Фамилия пользователя Example: Glen
     * @queryParam email required string Email пользователя Example: john_glen@example.com
     *
     * @responseFile api/users/create-response.json
     *
     * @param CreateRequest $request
     * @param UserService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, UserService $service)
    {
        $user = $service->create(
            $request->getName(),
            $request->getSurname(),
            $request->getEmail()
        );

        UserResource::wrap('user');

        return UserResource::make($user);
    }

    /**
     * Update
     * Обновить пользователя
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID пользователя Example: 11
     * @queryParam name required string Имя пользователя Example: John
     * @queryParam surname required string Фамилия пользователя Example: Smith
     * @queryParam email required string Email пользователя Example: john_smith@example.com
     *
     * @responseFile api/users/update-response.json
     *
     * @param UpdateRequest $request
     * @param UserService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, UserService $service)
    {
        $user = $service->update(
            $request->getId(),
            $request->getName(),
            $request->getSurname(),
            $request->getEmail()
        );

        UserResource::wrap('user');

        return UserResource::make($user);
    }
}
