<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Resources\TaskTypes\TaskTypeResource;
use App\Services\TaskTypes\TaskTypeService;

/**
 * Class TaskTypeController
 * @package App\Http\Controllers\Api
 *
 * @group Task Types
 * Методы для работы с типами задач
 */
class TaskTypeController extends Controller
{
    /**
     * Get List
     * Получить список типов задач
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     *
     * @responseFile api/task-types/get-list-response.json
     *
     * @param TaskTypeService $service
     *
     * @return mixed
     */
    public function getList(TaskTypeService $service)
    {
        $taskTypes = $service->getLIst();

        TaskTypeResource::wrap('types');

        return TaskTypeResource::collection($taskTypes);
    }
}
