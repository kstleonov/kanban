<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Components\CreateRequest;
use App\Requests\Components\DeleteRequest;
use App\Requests\Components\GetByIdRequest;
use App\Requests\Components\GetListRequest;
use App\Requests\Components\UpdateRequest;
use App\Resources\Components\ComponentResource;
use App\Services\Components\ComponentService;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ComponentController
 * @package App\Http\Controllers\Api
 * @group Components
 *
 * Методы для работы с компонентами задач
 */
class ComponentController extends Controller
{
    /**
     * Get By Id
     * Получить компонент по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID компонента Example: 2
     *
     * @responseFile api/components/get-by-id-response.json
     *
     * @param GetByIdRequest $request
     * @param ComponentService $service
     *
     * @return mixed
     */
    public function getById(GetByIdRequest $request, ComponentService $service)
    {
        $component = $service->getById($request->getId(), $this->user()->getId());

        ComponentResource::wrap('component');

        return ComponentResource::make($component);
    }

    /**
     * Get List
     * Получить список компонентов
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: IOS
     * @queryParam per_page string Кол-во возвращаемых записей Example: 10
     *
     * @responseFile api/components/get-list-response.json
     *
     * @param GetListRequest $request
     * @param ComponentService $service
     *
     * @return mixed
     */
    public function getList(GetListRequest $request, ComponentService $service)
    {
        /** @var Builder $builder */
        $builder = $service->getList($this->user()->getId(), $request->getSearchString());

        ComponentResource::wrap('components');

        return ComponentResource::collection($builder->paginate($request->getPerPage()));
    }

    /**
     * Create
     * Создать компонент
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam title required string Название компонента Example: IOS
     * @queryParam description string Описание компонента Example: IOS component
     * @queryParam project_id required int ID проекта для компонента Example: 1
     *
     * @responseFile api/components/create-response.json
     *
     * @param CreateRequest $request
     * @param ComponentService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, ComponentService $service)
    {
        $component = $service->create(
            $this->user()->getId(),
            $request->getProjectId(),
            $request->getTitle(),
            $request->getDescription()
        );

        ComponentResource::wrap('component');

        return ComponentResource::make($component);
    }

    /**
     * Update
     * Обновить компонент
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID компонента Example: 1
     * @queryParam title required string Название компонента Example: IOS
     * @queryParam description string Описание компонента Example: IOS component
     * @queryParam owner_id int ID нового владельца компонента Example: 1
     *
     * @responseFile api/components/update-response.json
     *
     * @param UpdateRequest $request
     * @param ComponentService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, ComponentService $service)
    {
        $component = $service->update(
            $request->getId(),
            $this->user()->getId(),
            $request->getTitle(),
            $request->getDescription(),
            $request->getOwnerId()
        );

        ComponentResource::wrap('component');

        return ComponentResource::make($component);
    }

    /**
     * Delete
     * Удалить компонент
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID компонента Example: 1
     *
     * @responseFile api/components/delete-response.json
     *
     * @param DeleteRequest $request
     * @param ComponentService $service
     *
     * @return mixed
     */
    public function delete(DeleteRequest $request, ComponentService $service)
    {
        $service->delete($request->getId(), $this->user()->getId());

        return ['deleted' => true];
    }
}
