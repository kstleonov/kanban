<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Tasks\AddObserverRequest;
use App\Requests\Tasks\CreateRequest;
use App\Requests\Tasks\GetByIdRequest;
use App\Requests\Tasks\GetListRequest;
use App\Requests\Tasks\UpdateRequest;
use App\Resources\Tasks\GetListResource;
use App\Resources\Tasks\TaskResource;
use App\Services\Tasks\TaskService;

/**
 * Class TaskController
 * @package App\Http\Controllers\Api
 * @group Tasks
 * Методы для работы с задачами
 */
class TaskController extends Controller
{
    /**
     * Get By Id
     * Получить задачу по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID задачи Example: 1
     *
     * @responseFile api/tasks/get-by-id-response.json
     *
     * @param GetByIdRequest $request
     * @param TaskService $service
     *
     * @return mixed
     */
    public function getById(GetByIdRequest $request, TaskService $service)
    {
        $task = $service->getById($this->user()->getId(), $request->getId());

        TaskResource::wrap('task');

        return TaskResource::make($task);
    }

    /**
     * Get List
     * Получить список задач
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID задачи Example: 1
     * @queryParam search string Строка поиска Example: Test task
     * @queryParam status_id int ID статуса Example: 1
     * @queryParam type_id int ID типа задачи Example: 1
     * @queryParam priority_id int ID приоритета задачи Example: 1
     * @queryParam component_id int ID компонента задачи Example: 1
     * @queryParam per_page int Кол-во элементов на странице Example: 25
     *
     * @responseFile api/tasks/get-list-response.json
     *
     * @param GetListRequest $request
     * @param TaskService $service
     *
     * @return mixed
     */
    public function getList(GetListRequest $request, TaskService $service)
    {
        $tasks = $service->getList(
            $this->user()->getId(),
            $request->getSearchString(),
            $request->getStatusId(),
            $request->getTypeId(),
            $request->getPriorityId(),
            $request->getComponentId()
        );

        GetListResource::wrap('tasks');

        return GetListResource::collection($tasks->paginate($request->getPerPage()));
    }

    /**
     * Create
     * Создать задачу
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam title required string Название задачи Example: New Task
     * @queryParam description required string Описание задачи Example: Create new task
     * @queryParam status_id required int ID статуса Example: 1
     * @queryParam project_id required int ID проекта Example: 1
     * @queryParam type_id required int ID типа задачи Example: 1
     * @queryParam priority_id required int ID приоритета задачи Example: 1
     * @queryParam performer_id required int ID исполнителя Example: 9
     * @queryParam component_id int ID компонента задачи Example: 1
     * @queryParam initial_time int Время в минутах на исполнение задачи Example: 60
     * @queryParam left_time int Оставшееся время в минутах на исполнение задачи Example: 20
     * @queryParam tags_ids array ID тэгов задачи Example: [1, 2]
     *
     * @responseFile api/tasks/create-response.json
     *
     * @param CreateRequest $request
     * @param TaskService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, TaskService $service)
    {
        $task = $service->create(
            $this->user()->getId(),
            $request->getTitle(),
            $request->getDescription(),
            $request->getStatusId(),
            $request->getProjectId(),
            $request->getTypeId(),
            $request->getPriorityId(),
            $request->getPerformerId(),
            $request->getComponentId(),
            $request->getInitialTime(),
            $request->getLeftTime(),
            $request->getTaskTags()
        );

        TaskResource::wrap('task');

        return TaskResource::make($task);
    }

    /**
     * Update
     * Обновить задачу
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID задачи Example: 1
     * @queryParam title required string Название задачи Example: New Task
     * @queryParam description required string Описание задачи Example: Create new task
     * @queryParam status_id required int ID статуса Example: 1
     * @queryParam type_id required int ID типа задачи Example: 1
     * @queryParam priority_id required int ID приоритета задачи Example: 1
     * @queryParam performer_id required int ID исполнителя Example: 9
     * @queryParam component_id int ID компонента задачи Example: 1
     * @queryParam initial_time int Время в минутах на исполнение задачи Example: 60
     * @queryParam left_time int Оставшееся время в минутах на исполнение задачи Example: 20
     * @queryParam tags_ids array ID тэгов задачи Example: [1, 2]
     *
     * @responseFile api/tasks/update-response.json
     *
     * @param UpdateRequest $request
     * @param TaskService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, TaskService $service)
    {
        $task = $service->update(
            $this->user()->getId(),
            $request->getId(),
            $request->getTitle(),
            $request->getDescription(),
            $request->getStatusId(),
            $request->getTypeId(),
            $request->getPriorityId(),
            $request->getPerformerId(),
            $request->getComponentId(),
            $request->getInitialTime(),
            $request->getLeftTime(),
            $request->getTaskTags()
        );

        TaskResource::wrap('task');

        return TaskResource::make($task);
    }

    /**
     * Add observer
     * Добавить наблюдателя задачи
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID задачи Example: 1
     *
     * @responseFile api/tasks/add-observer-response.json
     *
     * @param AddObserverRequest $request
     * @param TaskService $service
     *
     * @return mixed
     */
    public function addObserver(AddObserverRequest $request, TaskService $service)
    {
        $task = $service->addObserver($this->user()->getId(), $request->getId());

        TaskResource::wrap('task');

        return TaskResource::make($task);
    }
}
