<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Resources\TaskStatuses\TaskStatusResource;
use App\Services\TaskStatuses\TaskStatusService;

/**
 * Class TaskStatusController
 * @package App\Http\Controllers\Api
 * @group Task Statuses
 *
 * Методы для работы со статусами задач
 */
class TaskStatusController extends Controller
{
    /**
     * Get List
     * Получить список статусов задач
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     *
     * @responseFile api/task-statuses/get-list-response.json
     *
     * @param TaskStatusService $service
     *
     * @return mixed
     */
    public function getList(TaskStatusService $service)
    {
        $statuses = $service->getList();

        TaskStatusResource::wrap('statuses');

        return TaskStatusResource::collection($statuses);
    }
}
