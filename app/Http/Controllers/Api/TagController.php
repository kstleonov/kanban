<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Tags\CreateRequest;
use App\Requests\Tags\DeleteRequest;
use App\Requests\Tags\GetByIdRequest;
use App\Requests\Tags\GetListRequest;
use App\Requests\Tags\UpdateRequest;
use App\Resources\Tags\TagResource;
use App\Services\Tags\TagService;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TagController
 * @package App\Http\Controllers\Api
 * @group Tags
 *
 * Методы для работы с тэгами
 */
class TagController extends Controller
{
    /**
     * Get By Id
     * Получить тэг по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID тэга Example: 1
     *
     * @responseFile api/tags/get-by-id-response.json
     *
     * @param GetByIdRequest $request
     * @param TagService $service
     *
     * @return mixed
     */
    public function getById(GetByIdRequest $request, TagService $service)
    {
        $tag = $service->getById($request->getId());

        TagResource::wrap('tag');

        return TagResource::make($tag);
    }

    /**
     * Get List
     * Получить список тэгов
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam search string Строка поиска Example: #sprint_1
     *
     * @responseFile api/tags/get-list-response.json
     *
     * @param GetListRequest $request
     * @param TagService $service
     *
     * @return mixed
     */
    public function getList(GetListRequest $request, TagService $service)
    {
        /** @var Builder $builder */
        $builder = $service->getList($request->getSearchString());

        TagResource::wrap('tags');

        return TagResource::collection($builder->paginate($request->getPerPage()));
    }

    /**
     * Create
     * Создать тэг
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam title required string Название тэга Example: sprint_1
     *
     * @responseFile api/tags/create-response.json
     *
     * @param CreateRequest $request
     * @param TagService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, TagService $service)
    {
        $tag = $service->create($request->getTitle());

        TagResource::wrap('tag');

        return TagResource::make($tag);
    }

    /**
     * Update
     * Обновить тэг по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID тэга Example: 1
     * @queryParam title required string Название тэга Example: sprint_2
     *
     * @responseFile api/tags/update-response.json
     *
     * @param UpdateRequest $request
     * @param TagService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, TagService $service)
    {
        $tag = $service->update($request->getId(), $request->getTitle());

        TagResource::wrap('tag');

        return TagResource::make($tag);
    }

    /**
     * Delete
     * Удалить тэг по ID
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID тэга Example: 1
     *
     * @responseFile api/tags/delete-response.json
     *
     * @param DeleteRequest $request
     * @param TagService $service
     *
     * @return mixed
     */
    public function delete(DeleteRequest $request, TagService $service)
    {
        $service->delete($request->getId());

        return ['deleted' => true];
    }
}
