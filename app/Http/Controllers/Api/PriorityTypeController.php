<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Resources\PriorityTypes\PriorityTypeResource;
use App\Services\PriorityTypes\PriorityTypeService;

/**
 * Class PriorityTypeController
 * @package App\Http\Controllers\Api
 *
 * @group Priority Types
 * Методы для работы с типами приоритетов задач
 */
class PriorityTypeController extends Controller
{
    /**
     * Get List
     * Получить список типов приоритетов задач
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     *
     * @responseFile api/priority-types/get-list-response.json
     *
     * @param PriorityTypeService $service
     *
     * @return mixed
     */
    public function getList(PriorityTypeService $service)
    {
        $types = $service->getList();

        PriorityTypeResource::wrap('types');

        return PriorityTypeResource::collection($types);
    }
}
