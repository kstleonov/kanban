<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Requests\Comments\CreateRequest;
use App\Requests\Comments\UpdateRequest;
use App\Resources\Comments\CommentResource;
use App\Services\Comments\CommentService;


/**
 * Class CommentController
 * @package App\Http\Controllers\Api
 *
 * @group Comments
 * Методы для работы с комментариями
 */
class CommentController extends Controller
{
    /**
     * Create
     * Создать комментарии
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam task_id required int ID задачи Example: 1
     * @queryParam text required string Текст комментаря Example: Comment text
     *
     * @responseFile api/comments/create-response.json
     *
     * @param CreateRequest $request
     * @param CommentService $service
     *
     * @return mixed
     */
    public function create(CreateRequest $request, CommentService $service)
    {
        $comment = $service->create($this->user()->getId(), $request->getTaskId(), $request->getText());

        CommentResource::wrap('comment');

        return CommentResource::make($comment);
    }

    /**
     * Update
     * Обновить комментарии
     *
     * @queryParam token required int Токен авторизации Example: {{api-token}}
     * @queryParam id required int ID комментария Example: 1
     * @queryParam text required string Текст комментаря Example: Comment text
     *
     * @responseFile api/comments/update-response.json
     *
     * @param UpdateRequest $request
     * @param CommentService $service
     *
     * @return mixed
     */
    public function update(UpdateRequest $request, CommentService $service)
    {
        $comment = $service->update($this->user()->getId(), $request->getId(), $request->getText());

        CommentResource::wrap('comment');

        return CommentResource::make($comment);
    }
}
