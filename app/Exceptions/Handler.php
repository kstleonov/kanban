<?php

namespace App\Exceptions;

use App\Services\ResponseFormatter;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    public function render($request, \Throwable $exception)
    {
        /** @var ResponseFormatter $formatter */
        $formatter = $this->container->get(ResponseFormatter::class);

        if ($exception instanceof ValidationException) {
            $errors = $exception->errors();
            $message = __($exception->getMessage());
            $parts = [$message];
            foreach ($errors as $subErrors) {
                foreach ($subErrors as $errorMessage) {
                    $parts[] = $errorMessage;
                }
            }

            return response()
                ->json(
                    $formatter->format(
                        $request,
                        [
                            'errors' => $errors,
                        ],
                        [
                            'message' => implode(';' . PHP_EOL, $parts),
                            'error'   => 1,
                        ]
                    )
                )->header('X-Wrap', '1');
        }

        return response()->json($formatter->formatException($request, $exception))->header('X-Wrap', '1');
    }
}
