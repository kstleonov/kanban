<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 */
class Tag extends Model
{
    use HasFactory;

    protected $table = 'tags';
    public $timestamps = false;
}
