<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TaskStatus
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 */
class TaskStatus extends Model
{
    use HasFactory;

    protected $table = 'task_statuses';
    public $timestamps = false;
}
