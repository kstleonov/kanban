<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Task
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $status_id
 * @property int $project_id
 * @property int $type_id
 * @property int $priority_id
 * @property int $component_id
 * @property int $performer_id
 * @property int $author_id
 * @property string $initial_time
 * @property string $left_time
 * @property string $created_at
 * @property string $updated_at
 *
 * @property TaskStatus $status
 * @property Project $project
 * @property TaskType $type
 * @property PriorityType $priority
 * @property Component $component
 * @property User $performer
 * @property User $author
 * @property User $observers
 * @property Comment $comments
 * @property Tag $tags
 */
class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    public function scopeProjectAvailable(Builder $builder, $user_id): Builder
    {
        return $builder->whereHas(
            'project.users',
            static function (Builder $builder) use ($user_id) {
                $builder->where('users.id', $user_id);
            }
        );
    }

    public function status(): HasOne
    {
        return $this->hasOne(
            TaskStatus::class,
            'id',
            'status_id'
        );
    }

    public function project(): HasOne
    {
        return $this->hasOne(
            Project::class,
            'id',
            'project_id'
        );
    }

    public function type(): HasOne
    {
        return $this->hasOne(
            TaskType::class,
            'id',
            'type_id'
        );
    }

    public function priority(): HasOne
    {
        return $this->hasOne(
            PriorityType::class,
            'id',
            'priority_id'
        );
    }

    public function component(): HasOne
    {
        return $this->hasOne(
            Component::class,
            'id',
            'component_id'
        );
    }

    public function performer(): HasOne
    {
        return $this->hasOne(
            User::class,
            'id',
            'performer_id'
        );
    }

    public function author(): HasOne
    {
        return $this->hasOne(
            User::class,
            'id',
            'author_id'
        );
    }

    public function observers(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            ObserverTask::class,
            'task_id',
            'observer_id'
        );
    }

    public function comments(): BelongsToMany
    {
        return $this->belongsToMany(
            Comment::class,
            CommentTask::class,
            'task_id',
            'comment_id'
        );
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(
            Tag::class,
            TaskTag::class,
            'task_id',
            'tag_id'
        );
    }
}
