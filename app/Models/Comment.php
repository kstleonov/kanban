<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Comment
 * @package App\Models
 *
 * @property int $id
 * @property int $author_id
 * @property string text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $author
 * @property Task $task
 */
class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';

    public function author(): HasOne
    {
        return $this->hasOne(
            User::class,
            'id',
            'author_id'
        );
    }

    public function task(): BelongsToMany
    {
        return $this->belongsToMany(
            Task::class,
            CommentTask::class,
            'comment_id',
            'task_id'
        );
    }
}
