<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ProjectUser
 * @package App\Models
 *
 * @property int $id
 * @property int $project_id
 * @property int $user_id
 *
 * @property User $users
 * @property Project $projects
 */
class ProjectUser extends Model
{
    use HasFactory;

    protected $table = 'project_user';
    public $timestamps = false;
}
