<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TaskType
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 */
class TaskType extends Model
{
    use HasFactory;

    protected $table = 'task_types';
    public $timestamps = false;
}
