<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Project
 * @package App\Modules\Project\Models
 *
 * @property int $id
 * @property string $title
 * @property int $owner_id
 *
 * @property User $users
 * @property User $owner
 */
class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';
    public $timestamps = false;

    public function scopeProjectAvailable(Builder $builder, $user_id): Builder
    {
        return $builder->whereHas(
            'users',
            static function (Builder $builder) use ($user_id) {
                $builder->where('users.id', $user_id);
            });
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            ProjectUser::class,
            'project_id',
            'user_id'
        );
    }

    public function owner(): HasOne
    {
        return $this->hasOne(
            User::class,
            'id',
            'owner_id'
        );
    }
}
