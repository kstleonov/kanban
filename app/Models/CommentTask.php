<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CommentTask
 * @package App\Models
 *
 * @property int $id
 * @property int $task_id
 * @property int $comment_id
 */
class CommentTask extends Model
{
    use HasFactory;

    protected $table = 'comment_task';
    public $timestamps = false;
}
