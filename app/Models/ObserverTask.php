<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class ObserverTask
 * @package App\Models
 *
 * @property int $id
 * @property int $task_id
 * @property int $observer_id
 */
class ObserverTask extends Model
{
    use HasFactory;

    protected $table = 'observer_task';
    public $timestamps = false;
}
