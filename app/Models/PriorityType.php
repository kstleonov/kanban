<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PriorityType
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 */
class PriorityType extends Model
{
    use HasFactory;

    protected $table = 'priority_types';
    public $timestamps = false;
}
