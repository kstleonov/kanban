<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $email
 *
 * @property Project $projects
 */
class User extends Authenticatable
{
    use HasFactory;

    protected $table = 'users';
    public $timestamps = false;

    public function getId(): Int
    {
        return $this->id;
    }

    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(
            Project::class,
            ProjectUser::class,
            'user_id',
            'project_id'
        );
    }
}
