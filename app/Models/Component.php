<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Component
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $owner_id
 * @property int $project_id
 *
 * @property Project $project
 * @property User $owner
 */
class Component extends Model
{
    use HasFactory;

    protected $table = 'components';
    public $timestamps = false;

    public function scopeUserProject(Builder $builder, $user_id): Builder
    {
        return $builder->whereHas(
            'project.users',
            static function (Builder $builder) use ($user_id) {
                $builder->where('users.id', $user_id);
            });
    }

    public function project(): HasOne
    {
        return $this->hasOne(
            Project::class,
            'id',
            'project_id'
        );
    }

    public function owner(): HasOne
    {
        return $this->hasOne(
            User::class,
            'id',
            'owner_id'
        );
    }
}
