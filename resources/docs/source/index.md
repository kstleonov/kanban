---
title: API Reference

language_tabs:
- bash

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8080/docs/collection.json)

<!-- END_INFO -->

#Comments

Методы для работы с комментариями
<!-- START_698462b3e5c1aa4950cc1c952a238cb0 -->
## Create
Создать комментарии

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/comments/create?token=%7B%7Bapi-token%7D%7D&task_id=1&text=Comment+text" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/comments\/create",
    "message": "",
    "error": 0,
    "data": {
        "comment": {
            "id": 11,
            "author": {
                "id": 2,
                "name": "Malvina",
                "surname": "Weissnat",
                "email": "davis.cale@example.net"
            },
            "text": "Сделать до завтра"
        }
    },
    "token": "2"
}
```

### HTTP Request
`POST api/comments/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `task_id` |  required  | int ID задачи
    `text` |  required  | string Текст комментаря

<!-- END_698462b3e5c1aa4950cc1c952a238cb0 -->

<!-- START_c98636a73d829779c8d15edfdeba8687 -->
## Update
Обновить комментарии

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/comments/update?token=%7B%7Bapi-token%7D%7D&id=1&text=Comment+text" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/comments\/update",
    "message": "",
    "error": 0,
    "data": {
        "comment": {
            "id": 12,
            "author": {
                "id": 2,
                "name": "Malvina",
                "surname": "Weissnat",
                "email": "davis.cale@example.net"
            },
            "text": "Сделать до 12:00"
        }
    },
    "token": "2"
}
```

### HTTP Request
`POST api/comments/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID комментария
    `text` |  required  | string Текст комментаря

<!-- END_c98636a73d829779c8d15edfdeba8687 -->

#Components


Методы для работы с компонентами задач
<!-- START_d5e2744d18bcfcedcd2cdabd88c2285f -->
## Get By Id
Получить компонент по ID

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/components/get-by-id?token=%7B%7Bapi-token%7D%7D&id=2" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/components\/get-by-id",
    "message": "",
    "error": 0,
    "data": {
        "component": {
            "id": 2,
            "title": "Frontend",
            "description": "Frontend component",
            "owner": {
                "id": 10,
                "name": "Abe",
                "surname": "Doyle",
                "email": "payton.mante@example.net"
            }
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/components/get-by-id`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID компонента

<!-- END_d5e2744d18bcfcedcd2cdabd88c2285f -->

<!-- START_fa9b8bf024c221ad675e8da074d47f68 -->
## Get List
Получить список компонентов

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/components/get-list?token=%7B%7Bapi-token%7D%7D&search=IOS&per_page=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/components\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "components": [
            {
                "id": 2,
                "title": "Frontend",
                "description": "Frontend component",
                "owner": {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            },
            {
                "id": 3,
                "title": "IOS",
                "description": "IOS component",
                "owner": {
                    "id": 1,
                    "name": "Dudley",
                    "surname": "Padberg",
                    "email": "lessie79@example.org"
                }
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/components\/get-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/components\/get-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/components\/get-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/components\/get-list",
            "per_page": 10,
            "to": 2,
            "total": 2
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/components/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска
    `per_page` |  optional  | string Кол-во возвращаемых записей

<!-- END_fa9b8bf024c221ad675e8da074d47f68 -->

<!-- START_be3dd748d34198331931f07dc1b96a98 -->
## Create
Создать компонент

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/components/create?token=%7B%7Bapi-token%7D%7D&title=IOS&description=IOS+component&project_id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/components\/create",
    "message": "",
    "error": 0,
    "data": {
        "component": {
            "id": 5,
            "title": "IOS",
            "description": "IOS component",
            "owner": {
                "id": 1,
                "name": "Dudley",
                "surname": "Padberg",
                "email": "lessie79@example.org"
            }
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/components/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `title` |  required  | string Название компонента
    `description` |  optional  | string Описание компонента
    `project_id` |  required  | int ID проекта для компонента

<!-- END_be3dd748d34198331931f07dc1b96a98 -->

<!-- START_197c37845b86a99be160c5664bdedcf8 -->
## Update
Обновить компонент

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/components/update?token=%7B%7Bapi-token%7D%7D&id=1&title=IOS&description=IOS+component&owner_id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/components\/create",
    "message": "",
    "error": 0,
    "data": {
        "component": {
            "id": 5,
            "title": "IOS",
            "description": "IOS component",
            "owner": {
                "id": 1,
                "name": "Dudley",
                "surname": "Padberg",
                "email": "lessie79@example.org"
            }
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/components/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID компонента
    `title` |  required  | string Название компонента
    `description` |  optional  | string Описание компонента
    `owner_id` |  optional  | int ID нового владельца компонента

<!-- END_197c37845b86a99be160c5664bdedcf8 -->

<!-- START_94372acab2737f8a4138121e57e91cf3 -->
## Delete
Удалить компонент

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/components/delete?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/components\/delete",
    "message": "",
    "error": 0,
    "data": {
        "deleted": true
    },
    "token": "1"
}
```

### HTTP Request
`POST api/components/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID компонента

<!-- END_94372acab2737f8a4138121e57e91cf3 -->

#Priority Types

Методы для работы с типами приоритетов задач
<!-- START_f06bc2201a6528a9616f2c327fe8edc6 -->
## Get List
Получить список типов приоритетов задач

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/priority-types/get-list?token=%7B%7Bapi-token%7D%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/priority-types\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "types": [
            {
                "id": 1,
                "title": "Low"
            },
            {
                "id": 2,
                "title": "Normal"
            },
            {
                "id": 3,
                "title": "Critical"
            },
            {
                "id": 4,
                "title": "Priority"
            }
        ]
    },
    "token": "1"
}
```

### HTTP Request
`GET api/priority-types/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации

<!-- END_f06bc2201a6528a9616f2c327fe8edc6 -->

#Projects

Методы для работы с проектами
<!-- START_2633ec89df957dd3380b9bbfe1961f5d -->
## Get By Id
Получить проект по ID

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/projects/get-by-id?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/get-by-id",
    "message": "",
    "error": 0,
    "data": {
        "project": {
            "id": 2,
            "title": "utProject",
            "owner": {
                "id": 10,
                "name": "Abe",
                "surname": "Doyle",
                "email": "payton.mante@example.net"
            },
            "members": [
                {
                    "id": 1,
                    "name": "Dudley",
                    "surname": "Padberg",
                    "email": "lessie79@example.org"
                },
                {
                    "id": 4,
                    "name": "Vivienne",
                    "surname": "Toy",
                    "email": "andres.wolff@example.com"
                },
                {
                    "id": 7,
                    "name": "Jacynthe",
                    "surname": "Fay",
                    "email": "vbogisich@example.org"
                },
                {
                    "id": 8,
                    "name": "Katrina",
                    "surname": "Adams",
                    "email": "lila53@example.org"
                },
                {
                    "id": 9,
                    "name": "Newton",
                    "surname": "Balistreri",
                    "email": "ubailey@example.net"
                },
                {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/projects/get-by-id`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID проекта

<!-- END_2633ec89df957dd3380b9bbfe1961f5d -->

<!-- START_49de0b1b28b0266bd78ffd5cfc74f1fa -->
## Get List
Получить список проектов

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/projects/get-list?token=%7B%7Bapi-token%7D%7D&search=Test&sorting[order]=desc&sorting[field]=title&per_page=25" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "projects": [
            {
                "id": 2,
                "title": "utProject",
                "owner": {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            },
            {
                "id": 1,
                "title": "cumqueProject",
                "owner": {
                    "id": 2,
                    "name": "Malvina",
                    "surname": "Weissnat",
                    "email": "davis.cale@example.net"
                }
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/projects\/get-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/projects\/get-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/projects\/get-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/projects\/get-list",
            "per_page": 10,
            "to": 2,
            "total": 2
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/projects/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска
    `sorting.order` |  optional  | string Порядок сортировки
    `sorting.field` |  optional  | string Поле для сортировки
    `per_page` |  optional  | int Кол-во элементов на странице

<!-- END_49de0b1b28b0266bd78ffd5cfc74f1fa -->

<!-- START_b27601a77f863afed18a6772fce9d0b0 -->
## Get Simple List
Получить простой список проектов

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/projects/get-simple-list?token=%7B%7Bapi-token%7D%7D&search=Test&per_page=25" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/get-simple-list",
    "message": "",
    "error": 0,
    "data": {
        "projects": [
            {
                "id": 1,
                "title": "cumqueProject"
            },
            {
                "id": 2,
                "title": "utProject"
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/projects\/get-simple-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/projects\/get-simple-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/projects\/get-simple-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/projects\/get-simple-list",
            "per_page": 10,
            "to": 2,
            "total": 2
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/projects/get-simple-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска
    `per_page` |  optional  | int Кол-во элементов на странице

<!-- END_b27601a77f863afed18a6772fce9d0b0 -->

<!-- START_90c9263e66882c539269ffe27e78f89c -->
## Create
Создать проект

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/projects/create?token=%7B%7Bapi-token%7D%7D&title=Test+Project" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/create",
    "message": "",
    "error": 0,
    "data": {
        "project": {
            "id": 4,
            "title": "Test Project",
            "owner": {
                "id": 1,
                "name": "Dudley",
                "surname": "Padberg",
                "email": "lessie79@example.org"
            },
            "members": [
                {
                    "id": 1,
                    "name": "Dudley",
                    "surname": "Padberg",
                    "email": "lessie79@example.org"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/projects/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `title` |  required  | string Назваие проекта

<!-- END_90c9263e66882c539269ffe27e78f89c -->

<!-- START_550b0d4bcab0a573bffcb89ac2df1105 -->
## Update
Обновить проект

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/projects/update?token=%7B%7Bapi-token%7D%7D&id=3&title=New+Project&owner_id=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/update",
    "message": "",
    "error": 0,
    "data": {
        "project": {
            "id": 3,
            "title": "New Project",
            "owner": {
                "id": 10,
                "name": "Abe",
                "surname": "Doyle",
                "email": "payton.mante@example.net"
            },
            "members": [
                {
                    "id": 1,
                    "name": "Dudley",
                    "surname": "Padberg",
                    "email": "lessie79@example.org"
                },
                {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/projects/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID проекта
    `title` |  required  | string Название проекта
    `owner_id` |  optional  | int ID нового владельца проекта

<!-- END_550b0d4bcab0a573bffcb89ac2df1105 -->

<!-- START_5c2b0f7f6a17733ae8d69be93b162e62 -->
## Add Members
Добавить пользователей в проект

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/projects/add-members?token=%7B%7Bapi-token%7D%7D&id=3&members=%5B1%2C+10%2C+4%5D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/add-members",
    "message": "",
    "error": 0,
    "data": {
        "project": {
            "id": 3,
            "title": "New Project",
            "owner": {
                "id": 10,
                "name": "Abe",
                "surname": "Doyle",
                "email": "payton.mante@example.net"
            },
            "members": [
                {
                    "id": 1,
                    "name": "Dudley",
                    "surname": "Padberg",
                    "email": "lessie79@example.org"
                },
                {
                    "id": 3,
                    "name": "Allie",
                    "surname": "Rosenbaum",
                    "email": "qmetz@example.net"
                },
                {
                    "id": 4,
                    "name": "Vivienne",
                    "surname": "Toy",
                    "email": "andres.wolff@example.com"
                },
                {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            ]
        }
    },
    "token": "10"
}
```

### HTTP Request
`POST api/projects/add-members`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID проекта
    `members` |  optional  | array ID добавляемых в проект пользователей

<!-- END_5c2b0f7f6a17733ae8d69be93b162e62 -->

<!-- START_c76e1f8dc936e9576f404556c4fa2498 -->
## Remove Members
Удалить пользователей из проекта

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/projects/remove-members?token=%7B%7Bapi-token%7D%7D&id=3&members=%5B1%2C+3%5D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/projects\/remove-members",
    "message": "",
    "error": 0,
    "data": {
        "project": {
            "id": 3,
            "title": "New Project",
            "owner": {
                "id": 10,
                "name": "Abe",
                "surname": "Doyle",
                "email": "payton.mante@example.net"
            },
            "members": [
                {
                    "id": 4,
                    "name": "Vivienne",
                    "surname": "Toy",
                    "email": "andres.wolff@example.com"
                },
                {
                    "id": 10,
                    "name": "Abe",
                    "surname": "Doyle",
                    "email": "payton.mante@example.net"
                }
            ]
        }
    },
    "token": "10"
}
```

### HTTP Request
`POST api/projects/remove-members`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID проекта
    `members` |  optional  | array ID удаляемых в проект пользователей

<!-- END_c76e1f8dc936e9576f404556c4fa2498 -->

#Tags


Методы для работы с тэгами
<!-- START_eaefbb8c57001e1eef9af13a14e85f42 -->
## Get By Id
Получить тэг по ID

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/tags/get-by-id?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tags\/get-by-id",
    "message": "",
    "error": 0,
    "data": {
        "tag": {
            "id": 1,
            "title": "#ab"
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/tags/get-by-id`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID тэга

<!-- END_eaefbb8c57001e1eef9af13a14e85f42 -->

<!-- START_d0f6983846196f970bbb52463566867b -->
## Get List
Получить список тэгов

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/tags/get-list?token=%7B%7Bapi-token%7D%7D&search=%23sprint_1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tags\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "tags": [
            {
                "id": 1,
                "title": "#ab"
            },
            {
                "id": 2,
                "title": "#quibusdam"
            },
            {
                "id": 3,
                "title": "#voluptatum"
            },
            {
                "id": 4,
                "title": "#doloremque"
            },
            {
                "id": 5,
                "title": "#tempora"
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/tags\/get-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/tags\/get-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/tags\/get-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/tags\/get-list",
            "per_page": 10,
            "to": 5,
            "total": 5
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/tags/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска

<!-- END_d0f6983846196f970bbb52463566867b -->

<!-- START_46bb145936056cbf976671e77f7e99cc -->
## Create
Создать тэг

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tags/create?token=%7B%7Bapi-token%7D%7D&title=sprint_1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tags\/create",
    "message": "",
    "error": 0,
    "data": {
        "tag": {
            "id": 6,
            "title": "#sprint_1"
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tags/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `title` |  required  | string Название тэга

<!-- END_46bb145936056cbf976671e77f7e99cc -->

<!-- START_6f7e588027e3cef25c3c3bd575f3a3b1 -->
## Update
Обновить тэг по ID

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tags/update?token=%7B%7Bapi-token%7D%7D&id=1&title=sprint_2" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tags\/update",
    "message": "",
    "error": 0,
    "data": {
        "tag": {
            "id": 1,
            "title": "sprint_2"
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tags/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID тэга
    `title` |  required  | string Название тэга

<!-- END_6f7e588027e3cef25c3c3bd575f3a3b1 -->

<!-- START_eb106fc9f6ba89e2f32923fb7f404092 -->
## Delete
Удалить тэг по ID

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tags/delete?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tags\/delete",
    "message": "",
    "error": 0,
    "data": {
        "deleted": true
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tags/delete`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID тэга

<!-- END_eb106fc9f6ba89e2f32923fb7f404092 -->

#Task Statuses


Методы для работы со статусами задач
<!-- START_4bb18999e8c904c0bf1ff8ce94a49ce4 -->
## Get List
Получить список статусов задач

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/task-statuses/get-list?token=%7B%7Bapi-token%7D%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/task-statuses\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "statuses": [
            {
                "id": 1,
                "title": "In queue"
            },
            {
                "id": 2,
                "title": "In work"
            },
            {
                "id": 3,
                "title": "Testing"
            },
            {
                "id": 4,
                "title": "Deployment"
            },
            {
                "id": 5,
                "title": "Close"
            }
        ]
    },
    "token": "1"
}
```

### HTTP Request
`GET api/task-statuses/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации

<!-- END_4bb18999e8c904c0bf1ff8ce94a49ce4 -->

#Task Types

Методы для работы с типами задач
<!-- START_0fa330707364d06026684c56eb6a49c7 -->
## Get List
Получить список типов задач

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/task-types/get-list?token=%7B%7Bapi-token%7D%7D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/task-types\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "types": [
            {
                "id": 1,
                "title": "Task"
            },
            {
                "id": 2,
                "title": "Error"
            },
            {
                "id": 3,
                "title": "Bug"
            }
        ]
    },
    "token": "1"
}
```

### HTTP Request
`GET api/task-types/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации

<!-- END_0fa330707364d06026684c56eb6a49c7 -->

#Tasks

Методы для работы с задачами
<!-- START_471e883eec1b69f7814b1a5d54922b75 -->
## Get By Id
Получить задачу по ID

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/tasks/get-by-id?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tasks\/get-by-id",
    "message": "",
    "error": 0,
    "data": {
        "task": {
            "id": 2,
            "title": "voluptasTask",
            "description": "Veniam fuga nihil voluptates eos. Corrupti modi quo sed. Autem et laboriosam necessitatibus quis. Dolores deleniti consequatur nostrum enim rem aliquam.",
            "status": {
                "id": 2,
                "title": "In work"
            },
            "project": {
                "id": 1,
                "title": "namProject",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                },
                "members": [
                    {
                        "id": 1,
                        "name": "Thurman",
                        "surname": "Bashirian",
                        "email": "cassin.elvis@example.org"
                    },
                    {
                        "id": 9,
                        "name": "Jaqueline",
                        "surname": "Kirlin",
                        "email": "vondricka@example.com"
                    }
                ]
            },
            "type": {
                "id": 1,
                "title": "Task"
            },
            "priority": {
                "id": 4,
                "title": "Priority"
            },
            "component": {
                "id": 4,
                "title": "Android",
                "description": "Android component",
                "owner": {
                    "id": 2,
                    "name": "Richie",
                    "surname": "Kohler",
                    "email": "levi.kassulke@example.net"
                }
            },
            "performer": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "author": {
                "id": 4,
                "name": "Mitchell",
                "surname": "Luettgen",
                "email": "hirthe.ressie@example.net"
            },
            "initial_time": 4,
            "left_time": 79,
            "created_at": "2021-04-07T05:35:42.000000Z",
            "updated_at": "2021-04-07T05:35:42.000000Z",
            "observers": [
                {
                    "id": 1,
                    "name": "Thurman",
                    "surname": "Bashirian",
                    "email": "cassin.elvis@example.org"
                },
                {
                    "id": 2,
                    "name": "Richie",
                    "surname": "Kohler",
                    "email": "levi.kassulke@example.net"
                },
                {
                    "id": 4,
                    "name": "Mitchell",
                    "surname": "Luettgen",
                    "email": "hirthe.ressie@example.net"
                },
                {
                    "id": 7,
                    "name": "Torey",
                    "surname": "Batz",
                    "email": "edmund.kirlin@example.net"
                },
                {
                    "id": 10,
                    "name": "Telly",
                    "surname": "Ortiz",
                    "email": "chammes@example.net"
                }
            ],
            "comments": [
                {
                    "id": 3,
                    "author": {
                        "id": 3,
                        "name": "Keagan",
                        "surname": "Gutkowski",
                        "email": "ajenkins@example.net"
                    },
                    "text": "Minus est qui et dolores reprehenderit. Sequi animi odio deserunt et. Animi id aut accusantium fugiat provident perspiciatis quia."
                },
                {
                    "id": 4,
                    "author": {
                        "id": 5,
                        "name": "Myrl",
                        "surname": "O'Reilly",
                        "email": "jarrett.moen@example.com"
                    },
                    "text": "Consequatur possimus illum dicta. Impedit quae facilis neque laborum rerum necessitatibus. Dolores sit et non ab dignissimos vitae autem in. Dignissimos provident ipsum animi quisquam."
                },
                {
                    "id": 7,
                    "author": {
                        "id": 2,
                        "name": "Richie",
                        "surname": "Kohler",
                        "email": "levi.kassulke@example.net"
                    },
                    "text": "Aliquam optio quia quos illum voluptates incidunt enim ut. Ipsum consequatur et ab omnis distinctio dolorum maxime."
                },
                {
                    "id": 8,
                    "author": {
                        "id": 5,
                        "name": "Myrl",
                        "surname": "O'Reilly",
                        "email": "jarrett.moen@example.com"
                    },
                    "text": "Quia occaecati doloribus corrupti. Rerum rerum qui sunt rerum quis soluta consequatur. Et architecto officiis voluptatem quis."
                },
                {
                    "id": 9,
                    "author": {
                        "id": 8,
                        "name": "Evalyn",
                        "surname": "Heaney",
                        "email": "jarred.ratke@example.org"
                    },
                    "text": "Laboriosam officia ab expedita. Accusantium magni est molestiae sequi cumque nobis. Ea quisquam voluptatum excepturi tempora autem. Enim quibusdam odit earum pariatur corrupti."
                },
                {
                    "id": 10,
                    "author": {
                        "id": 4,
                        "name": "Mitchell",
                        "surname": "Luettgen",
                        "email": "hirthe.ressie@example.net"
                    },
                    "text": "Vel iusto distinctio voluptatem laborum ut. Tempore illo at aut architecto quisquam ea. Tenetur perferendis ipsa quia quia."
                }
            ],
            "tags": [
                {
                    "id": 1,
                    "title": "#ipsum"
                },
                {
                    "id": 1,
                    "title": "#ipsum"
                },
                {
                    "id": 3,
                    "title": "#libero"
                },
                {
                    "id": 4,
                    "title": "#dolores"
                },
                {
                    "id": 5,
                    "title": "#explicabo"
                },
                {
                    "id": 5,
                    "title": "#explicabo"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/tasks/get-by-id`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID задачи

<!-- END_471e883eec1b69f7814b1a5d54922b75 -->

<!-- START_84c5716786e19e3cbfdb7b97969961c6 -->
## Get List
Получить список задач

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/tasks/get-list?token=%7B%7Bapi-token%7D%7D&id=1&search=Test+task&status_id=1&type_id=1&priority_id=1&component_id=1&per_page=25" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tasks\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "tasks": [
            {
                "id": 2,
                "title": "voluptasTask",
                "status": {
                    "id": 2,
                    "title": "In work"
                },
                "type": {
                    "id": 1,
                    "title": "Task"
                },
                "priority": {
                    "id": 4,
                    "title": "Priority"
                },
                "component": {
                    "id": 4,
                    "title": "Android",
                    "description": "Android component",
                    "owner": {
                        "id": 2,
                        "name": "Richie",
                        "surname": "Kohler",
                        "email": "levi.kassulke@example.net"
                    }
                },
                "performer": {
                    "id": 1,
                    "name": "Thurman",
                    "surname": "Bashirian",
                    "email": "cassin.elvis@example.org"
                },
                "author": {
                    "id": 4,
                    "name": "Mitchell",
                    "surname": "Luettgen",
                    "email": "hirthe.ressie@example.net"
                }
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/tasks\/get-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/tasks\/get-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/tasks\/get-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/tasks\/get-list",
            "per_page": 10,
            "to": 1,
            "total": 1
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/tasks/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID задачи
    `search` |  optional  | string Строка поиска
    `status_id` |  optional  | int ID статуса
    `type_id` |  optional  | int ID типа задачи
    `priority_id` |  optional  | int ID приоритета задачи
    `component_id` |  optional  | int ID компонента задачи
    `per_page` |  optional  | int Кол-во элементов на странице

<!-- END_84c5716786e19e3cbfdb7b97969961c6 -->

<!-- START_47a73d7a572a61217ab2d00f6a83edca -->
## Create
Создать задачу

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tasks/create?token=%7B%7Bapi-token%7D%7D&title=New+Task&description=Create+new+task&status_id=1&project_id=1&type_id=1&priority_id=1&performer_id=9&component_id=1&initial_time=60&left_time=20&tags_ids=%5B1%2C+2%5D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tasks\/create",
    "message": "",
    "error": 0,
    "data": {
        "task": {
            "id": 5,
            "title": "New Task",
            "description": "Create new task",
            "status": {
                "id": 1,
                "title": "In queue"
            },
            "project": {
                "id": 1,
                "title": "namProject",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                },
                "members": [
                    {
                        "id": 1,
                        "name": "Thurman",
                        "surname": "Bashirian",
                        "email": "cassin.elvis@example.org"
                    },
                    {
                        "id": 9,
                        "name": "Jaqueline",
                        "surname": "Kirlin",
                        "email": "vondricka@example.com"
                    }
                ]
            },
            "type": {
                "id": 1,
                "title": "Task"
            },
            "priority": {
                "id": 1,
                "title": "Low"
            },
            "component": {
                "id": 1,
                "title": "Backend",
                "description": "Backend component",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                }
            },
            "performer": {
                "id": 9,
                "name": "Jaqueline",
                "surname": "Kirlin",
                "email": "vondricka@example.com"
            },
            "author": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "initial_time": 60,
            "left_time": null,
            "created_at": "2021-04-10T07:36:09.000000Z",
            "updated_at": "2021-04-10T07:36:09.000000Z",
            "observers": [
                {
                    "id": 1,
                    "name": "Thurman",
                    "surname": "Bashirian",
                    "email": "cassin.elvis@example.org"
                },
                {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                }
            ],
            "comments": [],
            "tags": [
                {
                    "id": 3,
                    "title": "#libero"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tasks/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `title` |  required  | string Название задачи
    `description` |  required  | string Описание задачи
    `status_id` |  required  | int ID статуса
    `project_id` |  required  | int ID проекта
    `type_id` |  required  | int ID типа задачи
    `priority_id` |  required  | int ID приоритета задачи
    `performer_id` |  required  | int ID исполнителя
    `component_id` |  optional  | int ID компонента задачи
    `initial_time` |  optional  | int Время в минутах на исполнение задачи
    `left_time` |  optional  | int Оставшееся время в минутах на исполнение задачи
    `tags_ids` |  optional  | array ID тэгов задачи

<!-- END_47a73d7a572a61217ab2d00f6a83edca -->

<!-- START_957646b6c89023a2d05bc0bac0a84e68 -->
## Update
Обновить задачу

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tasks/update?token=%7B%7Bapi-token%7D%7D&id=1&title=New+Task&description=Create+new+task&status_id=1&type_id=1&priority_id=1&performer_id=9&component_id=1&initial_time=60&left_time=20&tags_ids=%5B1%2C+2%5D" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tasks\/update",
    "message": "",
    "error": 0,
    "data": {
        "task": {
            "id": 5,
            "title": "New  update Task",
            "description": "Update new task",
            "status": {
                "id": 2,
                "title": "In work"
            },
            "project": {
                "id": 1,
                "title": "namProject",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                },
                "members": [
                    {
                        "id": 1,
                        "name": "Thurman",
                        "surname": "Bashirian",
                        "email": "cassin.elvis@example.org"
                    },
                    {
                        "id": 9,
                        "name": "Jaqueline",
                        "surname": "Kirlin",
                        "email": "vondricka@example.com"
                    }
                ]
            },
            "type": {
                "id": 2,
                "title": "Error"
            },
            "priority": {
                "id": 2,
                "title": "Normal"
            },
            "component": {
                "id": 1,
                "title": "Backend",
                "description": "Backend component",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                }
            },
            "performer": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "author": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "initial_time": 60,
            "left_time": 20,
            "created_at": "2021-04-10T07:36:09.000000Z",
            "updated_at": "2021-04-10T07:55:16.000000Z",
            "observers": [
                {
                    "id": 1,
                    "name": "Thurman",
                    "surname": "Bashirian",
                    "email": "cassin.elvis@example.org"
                }
            ],
            "comments": [],
            "tags": [
                {
                    "id": 1,
                    "title": "#ipsum"
                },
                {
                    "id": 4,
                    "title": "#dolores"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tasks/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID задачи
    `title` |  required  | string Название задачи
    `description` |  required  | string Описание задачи
    `status_id` |  required  | int ID статуса
    `type_id` |  required  | int ID типа задачи
    `priority_id` |  required  | int ID приоритета задачи
    `performer_id` |  required  | int ID исполнителя
    `component_id` |  optional  | int ID компонента задачи
    `initial_time` |  optional  | int Время в минутах на исполнение задачи
    `left_time` |  optional  | int Оставшееся время в минутах на исполнение задачи
    `tags_ids` |  optional  | array ID тэгов задачи

<!-- END_957646b6c89023a2d05bc0bac0a84e68 -->

<!-- START_4ef88de3c6304bb5eb7b0d2517899cf8 -->
## Add observer
Добавить наблюдателя задачи

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/tasks/add-observer?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/tasks\/add-observer",
    "message": "",
    "error": 0,
    "data": {
        "task": {
            "id": 5,
            "title": "New  update Task",
            "description": "Update new task",
            "status": {
                "id": 2,
                "title": "In work"
            },
            "project": {
                "id": 1,
                "title": "namProject",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                },
                "members": [
                    {
                        "id": 1,
                        "name": "Thurman",
                        "surname": "Bashirian",
                        "email": "cassin.elvis@example.org"
                    },
                    {
                        "id": 9,
                        "name": "Jaqueline",
                        "surname": "Kirlin",
                        "email": "vondricka@example.com"
                    }
                ]
            },
            "type": {
                "id": 2,
                "title": "Error"
            },
            "priority": {
                "id": 2,
                "title": "Normal"
            },
            "component": {
                "id": 1,
                "title": "Backend",
                "description": "Backend component",
                "owner": {
                    "id": 9,
                    "name": "Jaqueline",
                    "surname": "Kirlin",
                    "email": "vondricka@example.com"
                }
            },
            "performer": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "author": {
                "id": 1,
                "name": "Thurman",
                "surname": "Bashirian",
                "email": "cassin.elvis@example.org"
            },
            "initial_time": 60,
            "left_time": 20,
            "created_at": "2021-04-10T07:36:09.000000Z",
            "updated_at": "2021-04-10T07:55:16.000000Z",
            "observers": [
                {
                    "id": 1,
                    "name": "Thurman",
                    "surname": "Bashirian",
                    "email": "cassin.elvis@example.org"
                }
            ],
            "comments": [],
            "tags": [
                {
                    "id": 1,
                    "title": "#ipsum"
                },
                {
                    "id": 4,
                    "title": "#dolores"
                }
            ]
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/tasks/add-observer`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID задачи

<!-- END_4ef88de3c6304bb5eb7b0d2517899cf8 -->

#Users


Методы для работы с пользователями
<!-- START_201686e72ca3bd077f3ad57bf2a7881a -->
## Get By Id
Получить пользователя по ID

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/users/get-by-id?token=%7B%7Bapi-token%7D%7D&id=1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/users\/get-by-id",
    "message": "",
    "error": 0,
    "data": {
        "id": 1,
        "name": "Irving",
        "surname": "Rempel",
        "email": "reuben52@example.com"
    },
    "token": "1"
}
```

### HTTP Request
`GET api/users/get-by-id`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID пользовтеля

<!-- END_201686e72ca3bd077f3ad57bf2a7881a -->

<!-- START_34852db277ee81df95ec1ea755f1849f -->
## Get List
Получить список пользователей

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/users/get-list?token=%7B%7Bapi-token%7D%7D&search=John&sorting[order]=ASC&sorting[field]=name&per_page=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/users\/get-list",
    "message": "",
    "error": 0,
    "data": {
        "users": [
            {
                "id": 1,
                "name": "Irving",
                "surname": "Rempel",
                "email": "reuben52@example.com"
            },
            {
                "id": 2,
                "name": "Godfrey",
                "surname": "Dickens",
                "email": "bhahn@example.com"
            },
            {
                "id": 3,
                "name": "Leopoldo",
                "surname": "Gleichner",
                "email": "mallie.kuhlman@example.net"
            },
            {
                "id": 4,
                "name": "Mikayla",
                "surname": "Miller",
                "email": "senger.rick@example.com"
            },
            {
                "id": 5,
                "name": "Damien",
                "surname": "Smitham",
                "email": "kpowlowski@example.com"
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/users\/get-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/users\/get-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/users\/get-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/users\/get-list",
            "per_page": 10,
            "to": 10,
            "total": 10
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/users/get-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска
    `sorting` |  optional  | array Колонки для сортировки
    `sorting.order` |  optional  | string Порядок сортировки
    `sorting.field` |  optional  | string Поле для сортировки
    `per_page` |  optional  | int Кол-во возвращаемых записей

<!-- END_34852db277ee81df95ec1ea755f1849f -->

<!-- START_c67a8824ac9b3125a8988501a337c148 -->
## Get Simple List
Получить простой список пользователей

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8080/api/users/get-simple-list?token=%7B%7Bapi-token%7D%7D&search=John&per_page=10" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/users\/get-simple-list",
    "message": "",
    "error": 0,
    "data": {
        "users": [
            {
                "id": 1,
                "name": "Irving",
                "surname": "Rempel"
            },
            {
                "id": 2,
                "name": "Godfrey",
                "surname": "Dickens"
            },
            {
                "id": 3,
                "name": "Leopoldo",
                "surname": "Gleichner"
            },
            {
                "id": 4,
                "name": "Mikayla",
                "surname": "Miller"
            },
            {
                "id": 5,
                "name": "Damien",
                "surname": "Smitham"
            }
        ],
        "links": {
            "first": "http:\/\/localhost:8080\/api\/users\/get-simple-list?page=1",
            "last": "http:\/\/localhost:8080\/api\/users\/get-simple-list?page=1",
            "prev": null,
            "next": null
        },
        "meta": {
            "current_page": 1,
            "from": 1,
            "last_page": 1,
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "http:\/\/localhost:8080\/api\/users\/get-simple-list?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "path": "http:\/\/localhost:8080\/api\/users\/get-simple-list",
            "per_page": 10,
            "to": 10,
            "total": 10
        }
    },
    "token": "1"
}
```

### HTTP Request
`GET api/users/get-simple-list`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `search` |  optional  | string Строка поиска
    `per_page` |  optional  | int Кол-во возвращаемых записей

<!-- END_c67a8824ac9b3125a8988501a337c148 -->

<!-- START_57d87049be86661f752d2ec9525fb3df -->
## Create
Создать пользователя

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/users/create?token=%7B%7Bapi-token%7D%7D&name=John&surname=Glen&email=john_glen%40example.com" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/users\/create",
    "message": "",
    "error": 0,
    "data": {
        "user": {
            "id": 11,
            "name": "John",
            "surname": "Glen",
            "email": "john_glen@example.com"
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/users/create`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `name` |  required  | string Имя пользователя
    `surname` |  required  | string Фамилия пользователя
    `email` |  required  | string Email пользователя

<!-- END_57d87049be86661f752d2ec9525fb3df -->

<!-- START_7ffaa4ecbfe5a8d857d5d84f593f85b7 -->
## Update
Обновить пользователя

> Example request:

```bash
curl -X POST \
    "http://localhost:8080/api/users/update?token=%7B%7Bapi-token%7D%7D&id=11&name=John&surname=Smith&email=john_smith%40example.com" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```


> Example response (200):

```json
{
    "command": "\/api\/users\/update",
    "message": "",
    "error": 0,
    "data": {
        "user": {
            "id": 11,
            "name": "John",
            "surname": "Smith",
            "email": "john_smith@example.com"
        }
    },
    "token": "1"
}
```

### HTTP Request
`POST api/users/update`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    `token` |  required  | int Токен авторизации
    `id` |  required  | int ID пользователя
    `name` |  required  | string Имя пользователя
    `surname` |  required  | string Фамилия пользователя
    `email` |  required  | string Email пользователя

<!-- END_7ffaa4ecbfe5a8d857d5d84f593f85b7 -->


